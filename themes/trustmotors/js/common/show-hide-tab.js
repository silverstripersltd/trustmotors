const Packery = require('packery');
function packeryLayout() {

    let pckry = new Packery( '.js-packery', {
        itemSelector: '.card-gallery',
        gutter: 0
    });

    pckry.layout();
}

$('#js-category1').on('click', function () {
    $('.js-cat1').show();
    $('.js-cat2').hide();
    $('.js-cat3').hide();
    packeryLayout();
});
$( document ).on('click', '#js-category2', function (e) {
    e.preventDefault();
    $('.js-cat2').show();
    $('.js-cat1').hide();
    $('.js-cat3').hide();
    packeryLayout();
});
$( document ).on('click', '#js-category3', function (e) {
    e.preventDefault();
    $('.js-cat3').show();
    $('.js-cat2').hide();
    $('.js-cat1').hide();
    packeryLayout();
});
