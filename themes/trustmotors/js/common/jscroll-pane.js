$(function() {

    $('.js-scroll-pane').jScrollPane({

        resizeSensor: true,
        horizontalDragMaxWidth: 75,

    });

});
