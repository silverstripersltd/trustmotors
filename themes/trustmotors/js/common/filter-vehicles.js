let make = '';
let model = '';
let year = '';
let fuel = '';
let body = '';
let transmission = '';
const vehicleGrid = document.getElementById('vehicles-grid');
const vehicleFilters = document.getElementById('vehicle-filters');
const vehicleModels = document.getElementById('vehicle-model');
const vehiclesResults = document.getElementsByClassName('js-vehicles-results')[0];
const vehiclesPaginationResult = document.getElementsByClassName('vehicles-pagination-result')[0];
const vehiclesPaginationBlock = document.getElementsByClassName('vehicles-pagination-block')[0];
const blockID = $('#showroom').attr('data-id');
const action = $('#showroom').attr('data-link')+"search/filter/"+blockID+"?";

function filter(url) {

    $.ajax({
        url: url,
        type: 'GET',

        success: function (response) {
            const result = JSON.parse(response);
            vehicleGrid.innerHTML = '';
            vehicleFilters.innerHTML = '';
            if (result.filters) {
                vehicleFilters.innerHTML = result.filters;
            }
            if (result.listings) {
                vehiclesResults.innerHTML = result.listings;
            } else {
                vehiclesResults.innerHTML = '<h3>No results found..</h3>';
            }
            if (vehiclesPaginationBlock) vehiclesPaginationBlock.innerHTML = '';
            if (vehiclesPaginationResult) vehiclesPaginationResult.innerHTML = result.pagination;
            window.bLazy.revalidate();
        },
        complete: function () {
        },
        error: function () {
        },
    });
}

function getModels(url) {
    $.ajax({
        url: url,
        type: 'GET',

        success: function (response) {
            const result = JSON.parse(response);
            vehicleModels.innerHTML = '';
            if (result.options) {
                vehicleModels.innerHTML = result.options;
            }
        },
        complete: function () {
        },
        error: function () {
        },
    });
}

function makeURL(base){
    return base+"BodyType="+body+"&&Make="+make+"&&Model="+model+"&&FuelType="+fuel+"&&Transmission="+transmission+"&&Year="+year;
}

$( document ).on('change', '.make', function () {
    make = $(this).val();
    filter(makeURL(action));
});
$( document ).on('change', '.model', function () {
    model = $(this).val();
    filter(makeURL(action));
});
$( document ).on('change', '.year', function () {
    year = $(this).val();
    filter(makeURL(action));
});
$( document ).on('change', '.trans', function () {
    transmission = $(this).val();
    filter(makeURL(action));
});
$( document ).on('change', '.bodytype', function () {
    body = $(this).val();
    filter(makeURL(action));
});
$( document ).on('change', '.fuel', function () {
    fuel = $(this).val();
    filter(makeURL(action));
});

$( document ).on('click', '.page-number', function (e) {
    e.preventDefault();
    if(!make && !model && !transmission && !year && !fuel && !body){
        let link = $(this).attr('href');
        filter(action+"s"+link.substring(link.indexOf('start') + 1));
    }else {
        filter($(this).attr('href'));
    }
});