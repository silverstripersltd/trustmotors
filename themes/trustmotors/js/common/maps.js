import L from "leaflet";
const maps = $('.js-maps');

if (maps.length) {


    window.initMaps = function(){

        let mapOptions = {
            zoom: 15,
            scrollwheel: false,
            navigationControl: false,
            mapTypeControl: false,
            scaleControl: true,
            draggable: true
        };

        let counter = 1;

        maps.each(function() {


            const mapDom = $(this);
            const address = mapDom.data('address');
            const id = 'map__' + counter;
            mapDom.html('<div class="map" id="' + id + '"></div>');

            const map = L.map(id, {
                zoom: 18
            });

            L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
                maxZoom: 18,
                id: 'nivankafonseka/ckpgaite120d917pa4zahf73t',
                tileSize: 512,
                zoomOffset: -1,
                accessToken: 'pk.eyJ1Ijoibml2YW5rYWZvbnNla2EiLCJhIjoiY2piZXVqYjZmMnVqODMzcWdnNHRtNnpldCJ9.KsvDzV4jUau13c_RclgMmw'
            }).addTo(map);

            $.get(location.protocol + '//nominatim.openstreetmap.org/search?format=json&q='+address, function(data){
                if (data) {
                    const location = data[0];
                    if (location.lat && location.lon) {
                        const position = [location.lat, location.lon];
                        const marker = map.setView(position);

                        L.marker(position).addTo(map);
                    }
                }
            });


            // const mapDom = $(this);
            // const id = 'map__' + counter;
            // const address = mapDom.data('address');
            // mapDom.html('<div class="map" id="' + id + '"></div>');
            //
            //
            // let map = new google.maps.Map(
            //     document.getElementById(id),
            //     mapOptions
            // );
            //
            //
            // let geocoder = new google.maps.Geocoder();
            // geocoder.geocode(
            //     {
            //         address: address,
            //         componentRestrictions: {
            //             country: 'NZ'
            //         }
            //     },
            //     function(results, status) {
            //         if (results && results.length >= 0) {
            //             let result = results[0];
            //             let latLng = new google.maps.LatLng(
            //                 result.geometry.location.lat(),
            //                 result.geometry.location.lng()
            //             );
            //             var marker = new google.maps.Marker({
            //                 position: {
            //                     lat: result.geometry.location.lat(),
            //                     lng: result.geometry.location.lng(),
            //                 }
            //             });
            //             marker.setMap(map);
            //             map.setCenter({
            //                 lat: result.geometry.location.lat(),
            //                 lng: result.geometry.location.lng(),
            //             });
            //         }
            //     }
            // );

            counter += 1;
        });

    };

    const src = '//maps.googleapis.com/maps/api/js?key=' + 'AIzaSyA75bvw8cx6CXEZ01v5K2FJ843-9FjnSSw' + '&callback=initMaps';
    const script = document.createElement('script');
    script.src = src;
    script.type = 'text/javascript';
    document.body.appendChild(script);



}