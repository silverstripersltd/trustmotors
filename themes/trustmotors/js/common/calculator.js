import noUiSlider from 'nouislider';

const connectSliderRepayment = document.getElementById('interest-value-repayment');
const connectSliderBorrow = document.getElementById('interest-value-borrow');


//repayment
var interest = 6.95;
var amount = 20000;
var term = 3;
var repayment_period = 52;
const termDiv = document.getElementById('loan-term');
const display = document.getElementById('calculator-result-display');
const amountDiv = document.getElementById('loan-amount');
amountDiv.value  = amount;



//borrorw
var borrow_repayments = 200;
var borrow_term = 3;
var borrow_period = 52;
var borrow_interest = 6.95;
const borrowTermDiv = document.getElementById('borrow-loan-term');
const available = document.getElementById('borrow-result-display');
const borrowAmountDiv = document.getElementById('borrow-amount');
borrowAmountDiv.value  = borrow_repayments;

if (connectSliderRepayment) {
    noUiSlider.create(connectSliderRepayment, {
        start: 6.95,
        step: 1,
        tooltips: [true],
        range: {
            'min': [6.95],
            'max': [19.95]
        }
    });
    connectSliderRepayment.noUiSlider.on('update', repayment);
}

if (connectSliderBorrow) {
    noUiSlider.create(connectSliderBorrow, {
        start: 6.95,
        step: 1,
        tooltips: [true],
        range: {
            'min': [6.95],
            'max': [19.95]
        }
    });
    connectSliderBorrow.noUiSlider.on('update', available_loan);
}

function repayment(values, handle, unencoded, tap, positions, noUiSlider) {
    interest = unencoded[0];
    calculateRepayment();
}

function available_loan(values, handle, unencoded, tap, positions, noUiSlider) {
    borrow_interest = unencoded[0];
    calculateBorrow();
}


amountDiv.addEventListener('keyup', (event) => {
    amount = event.target.value;
    calculateRepayment();
});

borrowAmountDiv.addEventListener('keyup', (event) => {
    borrow_repayments = event.target.value;
    calculateBorrow();
});

termDiv.addEventListener('change', function (event) {
    term = event.target.value;
    calculateRepayment();
});

borrowTermDiv.addEventListener('change', function (event) {
    borrow_term = event.target.value;
    calculateBorrow();
});


const periods = document.getElementsByName('repayment-period');
for(var x=0; x < periods.length; x++) {
    periods[x].addEventListener('click', function(e) {
        repayment_period = e.target.value;
        calculateRepayment();
    });
}

const borrow_periods = document.getElementsByName('borrow-period');
for(var x=0; x < borrow_periods.length; x++) {
    borrow_periods[x].addEventListener('click', function(e) {
        borrow_period = e.target.value;
        calculateBorrow();
    });
}

function calculateRepayment() {
    const interest_rate = parseFloat( interest ) / ( 100 * repayment_period );
    const loan_term = parseInt( term ) * repayment_period;
    const loan_amount = parseInt( amount );

    if ( interest_rate != 0.0 ) {

        var Q = Math.pow( 1 + interest_rate, loan_term );
        var results = -1 * ( -( interest_rate * ( Q * loan_amount ) ) / ( -1 + Q ) );

    } else if ( loan_term != 0.0 ) {

        // No interest rate but repayment period exists
        var results = -1 * ( -( loan_amount / loan_term ) );

    } else {
        var results = 0;
    }

    display.innerHTML  = '$' + results.toFixed(0).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, '$1,');
}

function calculateBorrow() {

    const repayment_period = borrow_period;
    const interest_rate = parseFloat( borrow_interest ) / ( 100 * repayment_period );

    const loan_term = parseInt( borrow_term ) * repayment_period;

    const repayment_amount = parseInt( borrow_repayments );

    if ( interest_rate != 0.0 ) {
        var Q = Math.pow( 1 + interest_rate, loan_term );
        var results = ( ( repayment_amount * ( ( -1 + Q ) ) ) / interest_rate ) / Q ;
    } else if ( loan_term != 0 ) {
        var results = repayment_amount * loan_term;
    } else {
        var results = 0;
    }

    available.innerHTML  = '$' + results.toFixed(0).replace(/(\d)(?=(\d{3})+(?:\.\d+)?$)/g, '$1,');
}
