$(function() {
    if($('#enquire-id') && $('#enquire-id').length > 0){
        let form = $('#enquire-id').find('form');
        const action = $('#enquire-id').attr('data-Link')+"vehicle/enquireForm";
        if(form){
            let clicked = false;
            form.on('submit', function(e) {
                e.preventDefault();
                let vehicleId = $('#enquire-id').attr('data-vehicleID');
                let name = $('input[name="name"]').val();
                let email = $('input[name="email"]').val();
                let phone = $('input[name="phone"]').val();
                let comment = $('textarea[name="comment"]').val();

                let data = {
                    vehicleId: vehicleId,
                    fullname: name,
                    email: email,
                    message: comment,
                    phone: phone,
                };
                if(!clicked){
                    $('#loader').show();
                    $.ajax({
                        url: action,
                        type: 'POST',
                        contentType: 'JSON',
                        dataType: 'JSON',
                        data: JSON.stringify(data),
                        cache: true,
                        success: function (response) {
                            if(response.Message == 'Success'){
                                $('#enquire-id').html('<p>We have successfully received your enquiry.</p>').css('background', '#f2faf2');
                                $('#loader').hide();

                            }else{
                                $('#enquire-id').html('<p>An error occurred. please submit again.</p>').css('background', '#fff2f2');
                            }
                        },
                        complete: function () {
                        },
                        error: function () {
                            $('#enquire-id').html('<p>Internal server error occurred. please submit again.</p>').css('background', '#fff2f2');
                        },
                    });
                }
                clicked = true;
            });

        }
    }
});