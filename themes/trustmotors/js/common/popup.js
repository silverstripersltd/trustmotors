
$( document ).on('click', '.js-play-video', function () {

    $('.js-video-modal').addClass('active');

});

$( document ).on('click', '.js-close-button', function () {

    $('#videomodal').find('iframe').remove();

    $('.js-video-modal').removeClass('active');

});

$( document ).on('click', '.media-modal', function () {
    let iframe = $(this).attr('data-media');
    $('#videomodal').find('.js-video').append(iframe);
    $('.js-video-modal').addClass('active');
    $('.js-loading').addClass('hide');
});