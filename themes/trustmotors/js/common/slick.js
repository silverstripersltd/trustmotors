const slides = $('.js-slider');
const vehicleSlides = $('.js-vehicle-slider');
const carousels = $('.js-carousels');
const logoCarousel = $('.js-logo-carousels');
const Blazy = require('blazy');

const bLazy = new Blazy({
    loadInvisible: false,
    offset: 500,
    selector: '.b-lazy',
    breakpoints: [
        {
            width: 640, // max-width
            src: 'data-src-small',
        },
        {
            width: 1024, // max-width
            src: 'data-src-medium',
        },
    ],
});

slides.each(function() {

    const slide = $(this);

    slide.slick({
        dots: true,
        arrows: true,
        fade: false,
        autoplay: true,
        autoplaySpeed: 3000,
        infinite: true,
        appendArrows: $(this).siblings('.js-slider-arrow'),
        appendDots: $(this).siblings('.js-slider-dots'),
    });

    $(this).on('afterChange', function() {

        bLazy.revalidate();

    });

});

vehicleSlides.each(function() {

    const slide = $(this);

    slide.slick({
        dots: false,
        arrows: true,
        fade: false,
        autoplay: false,
        infinite: false,
        appendArrows: $(this).siblings('.js-slider-arrow'),
    });

    $(this).on('afterChange', function() {

        bLazy.revalidate();

    });

});

carousels.each(function() {
    const scrolls = $(this).find('.js-horizontal');
    if (scrolls && scrolls.length) {
        $(this).on('init', function() {
            scrolls.jScrollPane();
        })
    }
});

carousels.each(function() {

    const carousel = $(this);
    carousel.slick({
        infinite: true,
        autoplay: true,
        autoplaySpeed: 2000,
        dots: true,
        arrows: false,
        slidesToShow: 2,
        slidesToScroll: 1,
        centerMode: false,
        appendDots: $(this).siblings('.js-carousel-dots'),
        responsive: [
            {
                breakpoint: 640,
                infinite: false,
                settings: {
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerMode: true,
                },
            },
        ],
    });

    $(this).on('afterChange', function() {

        bLazy.revalidate();

    });

});

logoCarousel.each(function() {

    const carousel = $(this);
    carousel.slick({
        infinite: true,
        autoplay: false,
        autoplaySpeed: 2000,
        dots: false,
        arrows: true,
        slidesToShow: 5,
        slidesToScroll: 1,
        centerMode: false,
        appendArrows: $(this).siblings('.js-logo-carousel-arrows'),
        responsive: [
            {
                breakpoint: 900,
                infinite: false,
                settings: {
                    infinite: true,
                    slidesToShow: 4,
                    slidesToScroll: 1,
                    centerMode: false,
                },
            },
            {
                breakpoint: 768,
                infinite: false,
                settings: {
                    infinite: true,
                    slidesToShow: 3,
                    slidesToScroll: 1,
                    centerMode: false,
                },
            },
            {
                breakpoint: 640,
                infinite: false,
                settings: {
                    infinite: true,
                    slidesToShow: 2,
                    slidesToScroll: 1,
                    centerMode: false,
                },
            },
            {
                breakpoint: 480,
                infinite: false,
                settings: {
                    infinite: true,
                    slidesToShow: 1,
                    slidesToScroll: 1,
                    centerMode: false,
                },
            },
        ],
    });

    $(this).on('afterChange', function() {

        bLazy.revalidate();

    });

});
