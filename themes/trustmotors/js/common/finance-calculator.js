$(function() {
    if($('#calculator') && $('#calculator').length > 0){
        let rate = $('#calculator').find('#rate');
        let repayment = $('#calculator').find('#repayment');
        let rateVal = $('#calculator').attr('data-rate');
        let priceVal = $('#calculator').attr('data-price');
        let additionalCost = $('#calculator').attr('data-addcost');
        let years = $('#calculator').find('#years');
        let yearVal = parseInt(years.attr('data-value'));
        rate.text(rateVal+'%');
        repayment.html('$'+calculate(priceVal, rateVal, yearVal, additionalCost)+'<span class="per"> per week*</span>');
        $( document ).on('click', '#term-minus',function(){
            if(yearVal > 1 && yearVal <= 50){
                years.text(yearVal-1+' Year(s)');
                yearVal = yearVal-1;
                repayment.html('$'+calculate(priceVal, rateVal, yearVal, additionalCost)+'<span class="per"> per week*</span>');
            }
        });
        $( document ).on('click', '#term-plus',function(){
            if(yearVal >= 1 && yearVal < 5){
                years.text(yearVal+1+' Year(s)');
                yearVal = yearVal+1;
                repayment.html('$'+calculate(priceVal, rateVal, yearVal, additionalCost)+'<span class="per"> per week*</span>');
            }
        });
    }

    function calculate(priceVal, rateVal, yearVal, cost){
        let vehiclePrice = parseFloat(priceVal);
        let additionalCost = parseFloat(cost);
        let price = vehiclePrice + additionalCost;
        console.log(price);
        let rate = parseFloat(rateVal);
        let term = parseInt(yearVal);

        let numberOfInstallments = 1;
        if (rate > 1.0) {
            rate = rate / 100.0;
        }
        rate /= 52;
        numberOfInstallments = term * 52;

        let pow = 1;
        for (let j = 0; j < numberOfInstallments; j++) {
            pow = pow * (1 + rate);
        }
        let xx = (price * pow * rate) / (pow - 1);
        let repayment = ((xx * 100) / 100);
        return parseFloat(repayment).toFixed(2);
    }
});