$('.js-hamburger').click(function hamburgerClick() {

    $('.js-mobile-menu').toggleClass('active');

    $('.js-hamburger').toggleClass('active');

    $('.js-header').toggleClass('open-menu');

    $('.js-menu-item').delay(200).each(function(i) {

        const $this = $(this);

        $this.delay(100 * i).queue(function() {

            $this.toggleClass('show');

        });

    });

});

$('.js-has-submenu, .js-sub-menu').mouseover(function() {

    $('.js-header').addClass('active-submenu');

});

$('.js-has-submenu, .js-sub-menu').mouseleave(function() {

    $('.js-header').removeClass('active-submenu');

});

// $(window).on('load',function() {
//
//     $('.js-sub-menu').addClass('page-load');
//
// });
