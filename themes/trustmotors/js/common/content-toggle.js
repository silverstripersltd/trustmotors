$(document.getElementsByClassName('js-expand')).on('click', function() {

    const $this = $(this);
    $this.parent().toggleClass('is-expand');
    $this.next('.js-more-content').slideToggle('slow');

});
