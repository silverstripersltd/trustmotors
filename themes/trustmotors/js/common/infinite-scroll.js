import {closeMoadal} from "./popup";

const InfiniteScroll = require('infinite-scroll');
const Packery = require('packery');
const holders = document.getElementsByClassName('js-paginator-holder');
const Blazy = require('blazy');

const bLazy = new Blazy({
    offset: 500,
    selector: '.lazy',
    loadInvisible: true,
    breakpoints: [
        {
            width: 640, // max-width
            src: 'data-src-small',
        },
        {
            width: 1024, // max-width
            src: 'data-src-medium',
        },
    ],
});

if (holders.length) {
    for (let holderId = 0; holderId < holders.length; holderId++) {
        const holder = holders[holderId];

        if(holder.classList.contains('init')) {
            continue;
        }

        let paginationLinks = document.querySelector(holder.dataset.button),
            endOfContent = document.querySelector(holder.dataset.endofcontent);
        let perPage = holder.dataset.perpage;
        let nextPage = holder.dataset.nextlink;

        let infScroll = new InfiniteScroll(holder, {
            path: function() {
                var link = nextPage;
                var current = ( this.loadCount + 1 ) * perPage;
                return link.replace(/(start=)[^\&]+/, '') + 'start=' + current;
            },
            append: holder.dataset.items,
            button: holder.dataset.button,
            checkLastPage: true,
            scrollThreshold: false,
            debug: false,
            history: false,
            status: holder.dataset.unitloading
        });


        infScroll.on('append', function( response, path, items ) {
            if (response.querySelector(holder.dataset.button)) {
                paginationLinks.style.display = 'inline-block';
            } else {
                if (endOfContent) {
                    endOfContent.style.display = 'inline-block';
                }
                paginationLinks.style.display = 'none';
            }
            bLazy.revalidate();
            // showMoadal();
            // closeMoadal();
            packeryLayout();
            setTimeout(function() {
                if (endOfContent) {
                    endOfContent.style.display = 'none';
                }
            }, 2000);

        });

        infScroll.on('request', function( response, path ) {
            paginationLinks.style.display = 'none';
        });

        holder.classList.add('init');
    }

}

function packeryLayout() {

    let pckry = new Packery( '.js-packery', {
        itemSelector: '.card-gallery',
        gutter: 0
    });

    pckry.layout();
}
