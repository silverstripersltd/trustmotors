const Blazy = require('blazy');

window.bLazy = new Blazy({
    offset: 500,
    selector: '.lazy',
    loadInvisible: true,
    breakpoints: [
        {
            width: 640, // max-width
            src: 'data-src-small',
        },
        {
            width: 1024, // max-width
            src: 'data-src-medium',
        },
    ],
});

// document.addEventListener("DOMContentLoaded", () => {
//     window.bLazy.revalidate();
// });

$( ".js-slider" ).ready( function () {
    let Blazy = new Blazy();
    Blazy.revalidate();
});

