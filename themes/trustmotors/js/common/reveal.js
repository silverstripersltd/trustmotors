import ScrollReveal from 'scrollreveal';

let scrollActionDone = false;

const slideUp = {
    distance: '5%',
    origin: 'bottom',
    opacity: 0,
    delay: 250,
};

window.slideUp = slideUp;

const slideLeft = {
    distance: '10%',
    origin: 'left',
    opacity: 0,
    delay: 250,
};

window.slideLeft = slideLeft;

const slideRight = {
    distance: '10%',
    origin: 'right',
    opacity: 0,
    delay: 250,
};

const slideRightForm = {
    distance: '10%',
    origin: 'right',
    opacity: 0,
    delay: 250,
};

window.slideRight = slideRight;

window.addEventListener('load', function onLoad() {

    ScrollReveal({ distance: '5%', delay: 250, interval: 350 });
    ScrollReveal().reveal('.reveal-up', slideUp);
    ScrollReveal().reveal('.reveal-left', slideLeft);
    ScrollReveal().reveal('.reveal-right', slideRight);
    ScrollReveal().reveal('.reveal-right-form', slideRightForm);
});
