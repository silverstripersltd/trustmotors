if (document.readyState === 'complete') {

    $(document).foundation();

} else {

    window.addEventListener('load', function foundationInit() {

        $(document).foundation();

    }, 0);

}
