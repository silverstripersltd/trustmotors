var gulp         = require('gulp');
var autoprefixer = require('gulp-autoprefixer');
var eslint       = require('gulp-eslint');
var concat = require('gulp-concat');
var inject       = require('gulp-inject-string');
var rename       = require('gulp-rename');
var replace      = require('gulp-replace');
var sass         = require('gulp-sass');
var sourcemaps   = require('gulp-sourcemaps');
var stylelint    = require('gulp-stylelint');
var uglify       = require('gulp-uglify');
var webpack    = require('webpack-stream');
var babel = require('gulp-babel');

var foundationSCSS = [
    'node_modules/normalize.scss/sass',
    'node_modules/foundation-sites/scss',
    'node_modules/motion-ui/src',
];

var appSCSS = [
    './scss/components/**/*.scss',
    './scss/mixins/**/*.scss',
    './scss/shared/**/*.scss',
];

const commonJS = [
    'js/common.js',
];

const foundationJS = [
    'node_modules/jquery/dist/jquery.js',

    // 'node_modules/what-input/dist/what-input.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.core.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.util.keyboard.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.util.triggers.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.util.box.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.util.mediaQuery.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.util.touch.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.util.imageLoader.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.util.nest.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.util.motion.js',
// 'node_modules/foundation-sites/dist/js/plugins/foundation.abide.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.accordion.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.accordionMenu.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.drilldown.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.dropdown.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.dropdownMenu.js',
// 'node_modules/foundation-sites/dist/js/plugins/foundation.equalizer.js',
// 'node_modules/foundation-sites/dist/js/plugins/foundation.interchange.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.offcanvas.js',
// 'node_modules/foundation-sites/dist/js/plugins/foundation.orbit.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.tabs.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.responsiveAccordionTabs.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.responsiveMenu.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.responsiveToggle.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.reveal.js',
// 'node_modules/foundation-sites/dist/js/plugins/foundation.slider.js',
// 'node_modules/foundation-sites/dist/js/plugins/foundation.magellan.js',
// 'node_modules/foundation-sites/dist/js/plugins/foundation.sticky.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.toggler.js',
// 'node_modules/foundation-sites/dist/js/plugins/foundation.tooltip.js',
    'node_modules/foundation-sites/dist/js/plugins/foundation.smoothScroll.js',

// Third Party Plugin
    'node_modules/slick-carousel/slick/slick.js',
    'node_modules/jscrollpane/script/jquery.mousewheel.js',
    'node_modules/jscrollpane/script/jquery.jscrollpane.min.js',
    'node_modules/@fancyapps/fancybox/dist/jquery.fancybox.js',
    // 'node_modules/jsmarquee/dist/marquee.js',
    // 'node_modules/smarquee/dist/smarquee.min.js',
    'js/foundation.js'
];

// Lint SCSS
gulp.task('scss-lint', function() {
    return gulp.src(appSCSS)
        .pipe(stylelint({
            reporters: [{
                formatter: 'string',
                console: true
            }]
        }));
});

// Generate & Minify CSS
gulp.task('scss-compile', function() {
    return gulp.src('scss/app.scss')
        .pipe(sourcemaps.init())
        .pipe(sass({
            includePaths: foundationSCSS,
            outputStyle: 'compressed'
        })
            .on('error', sass.logError))
        .pipe(rename({
            extname: '.min.css'
        }))
        .pipe(autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9']
        }))
        .pipe(sourcemaps.write('../sourcemaps'))
        .pipe(gulp.dest('dist'));
});

// Critical CSS
gulp.task('critical-compile', function() {
    return gulp.src('./scss/critical.scss')
        .pipe(sass({
            includePaths: foundationSCSS,
            outputStyle: 'compressed'
        })
            .on('error', sass.logError))
        .pipe(replace("../fonts", "{$AbsoluteBaseURL}{$ThemeDir}/fonts"))
        .pipe(replace("../images", "{$AbsoluteBaseURL}{$ThemeDir}/images"))
        .pipe(replace('\\f', '\\\\f'))
        .pipe(replace('\\e', '\\\\e'))
        .pipe(replace('\\0', '\\\\0'))
        .pipe(replace('\\2', '\\\\2'))
        .pipe(autoprefixer({
            browsers: ['last 2 versions', 'ie >= 9']
        }))
        .pipe(rename('CriticalCSS.ss'))
        .pipe(inject.wrap('<style>\n', '</style>'))
        .pipe(gulp.dest('./templates/Includes/'));
});

// Lint JS
gulp.task('js-lint', function() {
    return gulp.src('./js/**/*.js')
        .pipe(eslint({
            'configFile':'.eslintrc'
        }))
        .pipe(eslint.format());
});

// Generate & Minify JS
gulp.task('js-compile', function() {

    gulp.src(foundationJS)
        .pipe(concat('foundation.js'))
        .pipe(uglify())
        .pipe(gulp.dest('dist'));
    return gulp.src(commonJS)
        .pipe(webpack())
        .pipe(concat('common.js'))
        .pipe(babel({
            presets: [
                [
                    '@babel/preset-env',
                ],
            ],
        }))
        .pipe(uglify())
        .pipe(gulp.dest('dist'));

    // return exec('parcel build js/foundation.js js/common.js --no-source-maps', function(stdout, stderr) {
    //     if (stdout) {
    //         console.log(stdout);
    //     };
    //     if (stderr) {
    //         console.log(stderr);
    //     }
    // });
});

// SCSS and JS build tasks
gulp.task('scss', gulp.parallel('scss-lint', 'scss-compile', 'critical-compile'));
gulp.task('js', gulp.parallel('js-lint', 'js-compile'));

// Default
gulp.task('default', function() {
    gulp.watch('./scss/**/*.scss', { ignoreInitial: false }, gulp.parallel('scss'));
    gulp.watch('./js/**/*.js', { ignoreInitial: false }, gulp.parallel('js'));
});
