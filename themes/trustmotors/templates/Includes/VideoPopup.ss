<div class="video-popup js-video-modal" id="videomodal">
    <div class="wrapper">
        <div class="video-align js-video">
            <span class="loading js-loading"><h6>Loading...</h6></span>
        </div>
        <button class="close-button js-close-button" data-close aria-label="Close modal" type="button">
            <span aria-hidden="true">&times;</span>
        </button>
    </div>
</div>