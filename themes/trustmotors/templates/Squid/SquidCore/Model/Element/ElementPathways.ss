<div class="grid-container">
    <% if $Title && $ShowTitle %>
        <{$SelectedTitleTag} class="h2-block title title-line title-line--bottom-center large-text-center reveal-up">$Title</{$SelectedTitleTag}>
    <% end_if %>
    <% if $Content %>
        <div class="small-12 medium-12 large-10 large-offset-1 xlarge-8 xlarge-offset-2 large-text-center info reveal-up">
            $Content
        </div>
    <% end_if %>
    <div class="grid-x grid-margin-x wrapper">
        <% loop $Pathways.Sort('Sort') %>
            <div class="cell small-12 medium-6 large-6 card card--pathways reveal-up">
                <div class="card__image">
                    <% if $Link.LinkURL %>
                        <a class="image lazy"
                             href="{$Link.LinkURL}"
                             {$Link.TargetAttr}
                             style="background-image: url('{$Image.FocusFill(5, 5).URL}');"
                             data-src="{$Image.FocusFill(570, 200).URL}">
                        </a>
                    <% else %>
                        <div class="image lazy"
                             style="background-image: url('{$Image.FocusFill(5, 5).URL}');"
                             data-src="{$Image.FocusFill(570, 200).URL}">
                        </div>
                    <% end_if %>
                </div>
                <div class="card__content">
                    <span class="shape"></span>
                    <% if $Title %>
                        <h5 class="title h5-block">$Title</h5>
                    <% end_if %>
                    <% if $Content %>
                        <div class="info">
                            <p>$Content</p>
                        </div>
                    <% end_if %>
                    <% if $Link %>
                        $Link.setCSSClass('link link-has-arrow')
                    <% end_if %>
                </div>
            </div>
        <% end_loop %>
    </div>
</div>