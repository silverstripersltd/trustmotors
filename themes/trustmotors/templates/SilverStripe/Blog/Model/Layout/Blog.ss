<div class="blog-page ">
    <div class="grid-container">
        <div class="grid-x grid-margin-x">
            <div class="cell small-12 medium-12 large-12">
                <article class="blog-post-tag">
                    <div class="grid-x grid-margin-x js-paginator-holder"
                         data-items=".js-article-item"
                         data-button=".js-next-pagination"
                         data-unitloading=".js-unit-loading"
                         data-endofcontent=".js-end-of-content"
                         data-nextlink="{$PaginatedList.NextLink}"
                         data-perpage="{$PaginatedList.PageLength}">
                        <% if $PaginatedList.Exists %>
                            <% loop $PaginatedList %>
                                <% include SilverStripe\\Blog\\PostSummary %>
                            <% end_loop %>
                        <% else %>
                            <p><%t SilverStripe\\Blog\\Model\\Blog.NoPosts 'There are no posts' %></p>
                        <% end_if %>
                    </div>
                </article>
                <% if $PaginatedList.MoreThanOnePage && $PaginatedList.NotLastPage %>
                    <div class="button-align text-center">
                        <a href="{$PaginatedList.NextLink}" class="link link--large js-next-pagination">Load
                            More</a>
                    </div>
                    <div class="grid-x align-center">
                        <p class="infinite-scroll-last js-end-of-content align-center small subline"
                           style="display: none">End of content</p>
                    </div>
                    <div class="grid-x align-center unit-loading js-unit-loading" style="display: none">
                        <div class="button expanded clear margin-bottom-2 infinite-scroll-request grid-x align-center">
                            <p>Loading</p>
                        </div>
                    </div>
                <% end_if %>
            </div>
        </div>
    </div>
</div>
