<% if $MoreThanOnePage %>
    <nav aria-label="Pagination" class="cell small-12 medium-12 large-12 pagination-bar">
        <ul class="pagination text-center">
            <% if $NotFirstPage %>
                <li class="pagination-previous disabled"><a class="prev" href="{$PrevLink}">Previous</a></li>
            <% else %>
                <li class="pagination-previous disabled">Previous</li>
            <% end_if %>

            <% loop $PaginationSummary(4) %>
                <% if $CurrentBool %>
                    <li class="current"><span class="show-for-sr">You're on page</span> $PageNum</li>
                <% else %>
                    <% if $Link %>
                        <li><a href="$Link" aria-label="Page $PageNum">$PageNum</a></li>
                    <% else %>
                        <li class="ellipsis"></li>
                    <% end_if %>
                <% end_if %>
            <% end_loop %>

            <% if $NotLastPage %>
                <li class="pagination-next"><a href="{$NextLink}" aria-label="Next page">Next</a></li>
            <% else %>
                <li class="pagination-next disabled">Next</li>
            <% end_if %>
        </ul>
    </nav>
<% end_if %>
