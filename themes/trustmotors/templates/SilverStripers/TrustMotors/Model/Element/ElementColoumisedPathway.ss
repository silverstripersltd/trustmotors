<div class="grid-container">
<% if $Title && $ShowTitle %>
        <{$SelectedTitleTag} class="h2-block title large-text-center">$Title</{$SelectedTitleTag}>
<% end_if %>
</div>
<% if $PathwayObjects %>
    <% loop $PathwayObjects.Sort('Sort') %>
        <div class="pathway-column reveal-up <% if $HasBackground %>pathway-column--bg-black<% end_if %>">
            <div class="grid-container">
                <div class="small-12 medium-12 large-10 large-offset-1">
                    <div class="grid-x grid-margin-x ">
                        <% if $Title || $Content %>
                            <div class="cell small-12 medium-6 large-6 card-column-pathway">
                                <img class="top-shape" src="{$ThemeDir}/images/pathway-shape-small.svg" alt="">
                                <h4 class="h4-block title">$Title</h4>
                                <div class="info">
                                    $Content
                                </div>
                                <span class="link-has-arrow js-expand"><span class="more">Read more</span><span class="less">Read less</span></span>
                            </div>
                        <% end_if %>
                        <% if $Title2 || $Content2 %>
                            <div class="cell small-12 medium-6 large-6 card-column-pathway">
                                <img class="top-shape" src="{$ThemeDir}/images/pathway-shape-small.svg" alt="">
                                <h4 class="h4-block title">$Title2</h4>
                                <div class="info">
                                    $Content2
                                </div>
                                <span class="link-has-arrow js-expand"><span class="more">Read more</span><span class="less">Read less</span></span>
                            </div>
                        <% end_if %>
                    </div>
                </div>
            </div>
        </div>
    <% end_loop %>
<% end_if %>
