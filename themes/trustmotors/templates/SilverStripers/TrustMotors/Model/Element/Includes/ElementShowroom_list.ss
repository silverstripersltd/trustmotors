<% with $Block %>
    <% if $Vehicles %>
        <div class="js-vehicles-results">
            <div class="grid-x grid-margin-x wrapper js-vehicles-list" id="vehicles-grid">
                <% loop $Vehicles %>
                    <div class="cell small-12 medium-6 large-4 xlarge-3 card-showroom">
                        <a href='{$Top.getBaseLink}vehicle/view/$ID' class="card-showroom__image">
                            <% if $Images %>
                                <% with $Images.Sort('ID').first %>
                                    <div class="image lazy"
                                         style="background-image: url('$FocusFill(7,5).URL');"
                                         data-src="$URL" id="$ID">
                                    </div>
                                <% end_with %>
                            <% else %>
                                <div class="image lazy"
                                     style="background-image: url('$resourceURL('themes/trustmotors/images/cms/democar.jpg')');"
                                     data-src="$resourceURL('themes/trustmotors/images/cms/democar.jpg')">
                                </div>
                            <% end_if %>
                        </a>
                        <div class="card-showroom__content">
                            <h6 class="car-brand">$ModelYear.Title $Make.Title $Model.Title $Variant</h6>
                            <ul class="information">
                                <li>
                                    <span class="category">Kilometres</span>
                                    <span class="details">$Kms</span>
                                </li>
                                <li>
                                    <span class="category">Transmission</span>
                                    <span class="details">$Transmission.Title</span>
                                </li>
                                <li>
                                    <span class="category">Type</span>
                                    <span class="details">$BodyType.Title</span>
                                </li>
                                <li>
                                    <span class="category">Petrol</span>
                                    <span class="details">$FuelType.Title</span>
                                </li>
                            </ul>
                            <span class="price">$$Nice($Price)</span>
                        </div>
                    </div>
                <% end_loop %>
            </div>
        </div>
    <% else %>
        <p>There are no Vehicles found.</p>
    <% end_if %><% end_with %>