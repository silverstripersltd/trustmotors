<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 19/08/2020
 * Time: 08:33
 */

namespace SilverStripers\TrustMotors\Cronjob;


use GuzzleHttp\Client;
use SilverStripe\Assets\Folder;
use SilverStripe\Assets\Image;
use SilverStripe\Assets\Storage\AssetStore;
use SilverStripe\Assets\Storage\DBFile;
use SilverStripe\Control\Director;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\CronTask\Interfaces\CronTask;
use SilverStripe\SiteConfig\SiteConfig;
use SilverStripers\TrustMotors\Presentation\BodyType;
use SilverStripers\TrustMotors\Presentation\FuelType;
use SilverStripers\TrustMotors\Presentation\Make;
use SilverStripers\TrustMotors\Presentation\Model;
use SilverStripers\TrustMotors\Presentation\ModelYear;
use SilverStripers\TrustMotors\Presentation\Transmission;
use SilverStripers\TrustMotors\Presentation\Vehicle;

class VehicleDownloader implements CronTask
{
    public function getSchedule()
    {
        return "*/30 * * * *";
    }

    public function process()
    {
        return;
        ini_set('max_execution_time', '1800'); //300 seconds = 5 minutes
        $config = SiteConfig::current_site_config();
        if($config->VirtualYardAPI) {
            $client = new Client();
            $res = $client->request('GET',
                'https://dealers.virtualyard.com.au/api/v2/get.php?a=vehicles&key='.$config->VirtualYardAPI);
            if ($res->getStatusCode() == 200) {
                $jsonOutput = $res->getBody();
                $arrayFormat = \GuzzleHttp\json_decode($jsonOutput);
                if(count($arrayFormat->response) > 0){
                    $allIDs = [];
                    foreach($arrayFormat->response as $el){
                        $allIDs[] = $el->vehicleId;
                        $existingVehicle = Vehicle::get()->filter('VehicleId', $el->vehicleId)->first();
                        $data = [
                            'VehicleId' => $el->vehicleId,
                            'LastUpdate' => $el->lastUpdate,
                            'LocationName' => $el->locationName,
                            'Kms' => $el->kms,
                            'Variant' => $el->variant,
                            'AdvKms' => $el->advKms,
                            'PhotoPath' => $el->photoPath,
                            'Category' => $el->category,
                            'Type' => $el->type,
                            'EnginePower' => $el->enginePower,
                            'EngineMake' => $el->engineMake,
                            'EngineSize' => $el->engineSize,
                            'Description' => Vehicle::nl2p($el->description),
                            'ShortDescription' => $el->shortDescription,
                            'PrevPrice' => $el->prevPrice,
                            'Price' => $el->price,
                            'IsDriveAway' => $el->isDriveAway,
                            'Contact' => $el->contact,
                            'Status' => $el->status,
                            'RentalStatus' => $el->rentalStatus,
                        ];
                        if(!$existingVehicle){
                            $vehicle = Vehicle::create($data);

                            for ($x = 1; $x <= 50; $x++) {
                                if(property_exists($el, "photo".$x) && property_exists($el, "photoPath"))
                                {
                                    $name = 'photo'.$x;
                                    if($el->photoPath && $el->$name){
                                        $image = $this->importRemote($el->photoPath.$el->$name, $el->$name);
                                        $vehicle->Images()->add($image);
                                    }
                                } else {
                                    break;
                                }
                            }

                            $vehicle->BodyTypeID = $this->createBodyType($el->body);
                            $vehicle->FuelTypeID = $this->createFuelType($el->fuelType);
                            $vehicle->MakeID = $this->createMake($el->make);
                            $vehicle->ModelID = $this->createModel($el->model);
                            $vehicle->ModelYearID = $this->createModelYear($el->year);
                            $vehicle->TransmissionID = $this->createTransmission($el->transmission);
                            $vehicle->write();
                            $vehicle->publishRecursive();
                            echo "Created vehicle id: $el->vehicleId".(Director::is_cli() ? "\n" : '<br/>');
                        }else{
                            if($existingVehicle->LastUpdate != $el->lastUpdate){
                                $existingVehicle->update($data);
                                $updatedPropertyIDs = $this->updateProperties([
                                    'Make'=> $el->make,
                                    'Model' => $el->model,
                                    'ModelYear' => $el->year,
                                    'FuelType' => $el->fuelType,
                                    'BodyType' => $el->body,
                                    'Transmission' => $el->transmission,
                                ]);
                                foreach ($updatedPropertyIDs as $property => $id){
                                    if($id != null){
                                        $field = $property.'ID';
                                        $existingVehicle->$field = $id;
                                    }
                                }

                                for ($x = 1; $x <= 50; $x++) {
                                    if(property_exists($el, "photo".$x) && property_exists($el, "photoPath")){
                                        $name = 'photo'.$x;
                                        if($el->photoPath && $el->$name){
                                            if($existingVehicle->Images()->filter('Name', $el->$name)->count() == 0) {
                                                $image = $this->importRemote($el->photoPath.$el->$name, $el->$name);
                                                $existingVehicle->Images()->add($image);
                                            }
                                        }
                                    } else {
                                        break;
                                    }
                                }
                                $existingVehicle->write();
                                $existingVehicle->publishRecursive();
                                echo "Vehicle with id : $el->vehicleId is updated.".(Director::is_cli() ? "\n" : '<br/>');
                            }else{
                                echo "Vehicle with id : $el->vehicleId is already exists.".(Director::is_cli() ? "\n" : '<br/>');
                            }
                        }
                    }

                    $result = array_diff(Vehicle::get()->filter('Hide', 0)->column('VehicleId'),$allIDs);
                    if(!empty($result)){
                        $oldVehicles = Vehicle::get()->filter(['VehicleId' => $result, 'Hide' => 0]);
                        $this->hideOldOnes($oldVehicles);
                    }


                }
                return 'Success';
            }else {

                return 'Failed';
            }
        }

        echo 'Please try again after adding the API key to the CMS Site config..';
    }

    public function hideOldOnes($vehicles) {
        foreach ($vehicles as $vehicle) {
            $vehicle->Hide = 1;
            $vehicle->write();
            $vehicle->publishRecursive();
        }
    }

    public function importRemote($url='', $title='')
    {
        $this->RemoteURL = $url;
        if($url) {
            //Remove query string
            $url = (strpos($this->RemoteURL, '?') !== false) ?
                substr($this->RemoteURL, 0, strpos($this->RemoteURL, '?')) : $this->RemoteURL;
            $url = implode('/', array_map(function($element) {
                return implode(':', array_map('rawurlencode', explode(':', $element)));
            }, explode('/', $url)));
            $pathInfo = pathinfo($url);
            $parent = Folder::find_or_make('VehicleImages');
            $curl = curl_init();
            curl_setopt_array($curl, array(
                CURLOPT_URL => $url,
                CURLOPT_HEADER => 0,
                CURLOPT_RETURNTRANSFER => 1,
                CURLOPT_BINARYTRANSFER => 1
            ));
            $rawImage = curl_exec($curl);
            $resultInfo = curl_getinfo($curl);
            if($resultInfo['http_code'] !== 200) {
                echo "Failed to retrieve remote image $url." . (Director::is_cli() ? "\n" : '<br/>');
                return false;
            }
            $tmpFile = tmpfile();
            fwrite($tmpFile, $rawImage);
            fseek($tmpFile, 0);
            $assetStore = Injector::inst()->get(AssetStore::class);
            $name = $pathInfo['filename'].'.'. strtolower($pathInfo['extension']);
            $result = $assetStore->setFromStream($tmpFile, $name);
            $image = Image::create();
            $image->Name = $name;
            $image->Title = $title ? $title : $pathInfo['filename'];
            $image->ParentID = $parent->ID;
            $image->File = DBFile::create_field('DBFile', $result);
            $image->write();
            $image->publishSingle();
            fclose($tmpFile);
            return $image;
        }
    }

    public function createBodyType($type){
        if($type){
            $body = BodyType::get()->filter('Title', $type)->first();
            if(!$body){
                $body = BodyType::create();
                $body->Title = $type;
                $id = $body->write();
                return $id;
            }else{
                return $body->ID;
            }
        }else{
            return false;
        }
    }

    public function createFuelType($type){
        if($type){
            $fuel = FuelType::get()->filter('Title', $type)->first();
            if(!$fuel){
                $fuel = FuelType::create();
                $fuel->Title = $type;
                $id = $fuel->write();
                return $id;
            }else{
                return $fuel->ID;
            }
        }else{
            return false;
        }
    }

    public function createMake($type){
        if($type){
            $make = Make::get()->filter('Title', $type)->first();
            if(!$make){
                $make = Make::create();
                $make->Title = $type;
                $id = $make->write();
                return $id;
            }else{
                return $make->ID;
            }
        }else{
            return false;
        }
    }

    public function createModel($type){
        if($type){
            $model = Model::get()->filter('Title', $type)->first();
            if(!$model){
                $model = Model::create();
                $model->Title = $type;
                $id = $model->write();
                return $id;
            }else{
                return $model->ID;
            }
        }else{
            return false;
        }
    }

    public function createModelYear($type){
        if($type){
            $year = ModelYear::get()->filter('Title', $type)->first();
            if(!$year){
                $year = ModelYear::create();
                $year->Title = $type;
                $id = $year->write();
                return $id;
            }else{
                return $year->ID;
            }
        }else{
            return false;
        }
    }

    public function createTransmission($type){
        if($type){
            $transmission = Transmission::get()->filter('Title', $type)->first();
            if(!$transmission){
                $transmission = Transmission::create();
                $transmission->Title = $type;
                $id = $transmission->write();
                return $id;
            }else{
                return $transmission->ID;
            }
        }else{
            return false;
        }
    }

    public function updateProperties($properties){
        $result = [];
        if(is_array($properties) && count($properties) > 0){
            foreach ($properties as $property => $val){
                $object = $this->getExistingProperty($property, $val);
                if($object){
                    $result[$property] = $object->ID;
                }else{
                    $id = $this->createNewProperty($property, $val);
                    $result[$property] = $id;
                }
            }
            return $result;
        }
        return $result;
    }

    public function createNewProperty($property, $val){
        switch($property){
            case 'Transmission': {
                return $this->createTransmission($val);
            }
            case 'BodyType': {
                return $this->createBodyType($val);
            }
            case 'FuelType': {
                return $this->createFuelType($val);
            }
            case 'Make': {
                return $this->createMake($val);
            }
            case 'Model': {
                return $this->createModel($val);
            }
            case 'ModelYear': {
                return $this->createModelYear($val);
            }
            default : {
                return null;
            }
        }
    }

    public function getExistingProperty($property, $val){
        switch($property){
            case 'Transmission': {
                return Transmission::get()->filter('Title', $val)->first();
            }
            case 'BodyType': {
                return BodyType::get()->filter('Title', $val)->first();
            }
            case 'FuelType': {
                return FuelType::get()->filter('Title', $val)->first();
            }
            case 'Make': {
                return Make::get()->filter('Title', $val)->first();
            }
            case 'Model': {
                return Model::get()->filter('Title', $val)->first();
            }
            case 'ModelYear': {
                return ModelYear::get()->filter('Title', $val)->first();
            }
            default : {
                return false;
            }
        }
    }
}