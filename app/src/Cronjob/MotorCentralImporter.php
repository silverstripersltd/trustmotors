<?php


namespace SilverStripers\TrustMotors\Cronjob;

use SilverStripe\Assets\File;
use SilverStripe\Assets\Folder;
use SilverStripe\Assets\Storage\AssetStore;
use SilverStripe\Control\Director;
use SilverStripe\Core\Config\Configurable;
use SilverStripe\Core\Convert;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\CronTask\Interfaces\CronTask;
use SilverStripe\ORM\FieldType\DBDatetime;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\Versioned\Versioned;
use SilverStripers\TrustMotors\Presentation\Vehicle;

class MotorCentralImporter implements CronTask
{
    use Configurable;

//    private static $folder_path = '/Users/nivankafonseka/SilverStripers/Work/smokeylemon/motorcentral';
    private static $folder_path = '/home/trustmotors/domains/trustmotors.co.nz/motorcentral';

    public function getSchedule()
    {
        return "*/30 * * * *";
    }

    public function process()
    {
        ini_set('max_execution_time', '1800'); //300 seconds = 5 minutes
        $xml = $this->getXML();
        $arr = Convert::xml2array($xml);



        $downloader = new VehicleDownloader();

        $stage = Versioned::get_stage();
        Versioned::set_stage(Versioned::DRAFT);

        $path = self::config()->get('folder_path');
        $allFiles = scandir($path);

        $folder = Folder::find_or_make('motor-central');

        $vehicleIDs = [];

        if ($arr && !empty($arr['Vehicle']) && count($arr['Vehicle'])) {
            foreach ($arr['Vehicle'] as $arrVehicle) {

                $existingVehicle = Vehicle::get()->find('VehicleId', $arrVehicle['VehicleId']);
                if (!$existingVehicle) {
                    $existingVehicle = new Vehicle([
                        'VehicleId' => $arrVehicle['VehicleId']
                    ]);
                }

                $hash = md5(json_encode($arrVehicle));
                if ($existingVehicle->HashCode != $hash) {


                    $data = [
                        'HashCode' => $hash,
                        'VehicleId' => $arrVehicle['VehicleId'],
                        'LastUpdate' => DBDatetime::now()->getValue(),
                        'LocationName' => $arrVehicle['Dealership'], //
                        'Kms' => $arrVehicle['OdoReading'], //
                        'Variant' => $arrVehicle['Variant'], //
                        // 'AdvKms' => $arrVehicle['VehicleId'],
                        // 'PhotoPath' => $arrVehicle['VehicleId'],
                        // 'Category' => $arrVehicle['VehicleId'],
                        // 'Type' => $arrVehicle['VehicleId'],
                        'EnginePower' => $arrVehicle['EngineHP'],
//                        'EngineMake' => $arrVehicle['VehicleId'],
                        'EngineSize' => $arrVehicle['EngineSize'],
                        'Description' => Vehicle::nl2p($arrVehicle['Description']), //
                        'ShortDescription' => $arrVehicle['ShortDesc'], //
                        // 'PrevPrice' => $arrVehicle['VehicleId'],
                        'Price' => $arrVehicle['RetailPriceAmount'],
                        // 'IsDriveAway' => $arrVehicle['VehicleId'],
                        // 'Contact' => $arrVehicle['VehicleId'],
                        'Status' => $arrVehicle['VehicleStatus'], //
                        // 'RentalStatus' => $arrVehicle['VehicleId'],
                    ];

                    $existingVehicle->update($data);
                    $existingVehicle->write();


                    $existingVehicle->BodyTypeID = $downloader->createBodyType($arrVehicle['BodyStyle']);
                    $existingVehicle->FuelTypeID = $downloader->createFuelType($arrVehicle['FuelType1']);
                    $existingVehicle->MakeID = $downloader->createMake($arrVehicle['Manufacturer']);
                    $existingVehicle->ModelID = $downloader->createModel($arrVehicle['Model']);
                    $existingVehicle->ModelYearID = $downloader->createModelYear($arrVehicle['ManuYear']);
                    $existingVehicle->TransmissionID = $downloader->createTransmission($arrVehicle['Transmission']);
                    $existingVehicle->write();
                    echo "Vehicle with id : " . $arrVehicle['VehicleId'] . " is changed and updated.".(Director::is_cli() ? "\n" : '<br/>');

                } else {
                    echo "Vehicle with id : " . $arrVehicle['VehicleId'] . " is skipping only checking for images.".(Director::is_cli() ? "\n" : '<br/>');

                }

                // images
                foreach ($allFiles as $fileName) {
                    $filePath = $path . DIRECTORY_SEPARATOR . $fileName;
                    if (strpos($fileName, $arrVehicle['VehicleId']) === 0) {

                        $file = File::get()->find('ExternalID', $fileName);
                        if (!$file) {

                            $stream = fopen($filePath, 'r');

                            $extension = pathinfo($fileName, PATHINFO_EXTENSION);
                            $fileClass = File::get_class_for_file_extension(
                                $extension
                            );
                            /* @var File $file */
                            $file = Injector::inst()->create($fileClass);

                            /* @var $assetStore AssetStore */
                            $assetStore = Injector::inst()->get(AssetStore::class);
                            if ($result = $assetStore->setFromStream($stream, $fileName)) {
                                $dbFile = DBField::create_field('DBFile', $result);
                                if ($dbFile->exists()) {

                                    $file->update([
                                        'Name' => $fileName,
                                        'Title' => $fileName,
                                        'File' => $dbFile,
                                    ]);

                                    if ($folder) {
                                        $file->ParentID = $folder->ID;
                                    }
                                    $file->ExternalID = $fileName;
                                    $file->write();
                                    $file->publishRecursive();
                                }
                            }
                        }

                        if ($file) {
                            $existingVehicle->Images()->add($file->ID);
                        }
                    }
                }

                $vehicleIDs[] = $existingVehicle->ID;
                $existingVehicle->write();
                $existingVehicle->publishRecursive();


                echo "Vehicle with id : $existingVehicle->VehicleId is updated.".(Director::is_cli() ? "\n" : '<br/>');

            }
        }

        $downloader->hideOldOnes(Vehicle::get()->exclude('ID', $vehicleIDs));



    }

    private function getXMLPath()
    {
        return self::config()->get('folder_path') . DIRECTORY_SEPARATOR . 'vehicles.xml';
    }

    private function getXML()
    {
        $path = $this->getXMLPath();
        if (file_exists($path)) {
            return file_get_contents($path);
        }
    }

}