<?php
namespace SilverStripers\TrustMotors\DB;

use Psr\SimpleCache\CacheInterface;
use SilverStripe\Core\Injector\Injector;
use SilverStripe\ORM\FieldType\DBVarchar;
use SilverStripe\View\ArrayData;

class VideoVarchar extends DBVarchar {

    /**
     * @return bool|int
     */
    public function getID() {
        if ($this->IsYoutube()) {
            return self::youtubeId($this->value);
        } elseif ($this->IsVimeo()) {
            return self::vimeoId($this->value);
        }
        return 0;
    }

    /**
     * @return null|string
     */
    public function getType() {
        if ($this->IsYoutube()) {
            return 'youtube';
        } elseif ($this->IsVimeo()) {
            return strpos(parse_url($this->value, PHP_URL_HOST), 'vimeo') !== false;
        }
        return null;
    }

    /**
     * @param $iWidth
     * @param $iHeight
     * @return null|string
     */
    public function getTag($iWidth = 600, $iHeight = 400) {
        return '<iframe src="'.$this->getEmbedURL().'" class="responsive-video" width="'.$iWidth.'" height="'.$iHeight.'" allow="autoplay" frameborder="0" webkitallowfullscreen mozallowfullscreen allowfullscreen></iframe>';
    }

    /**
     * @return string
     */
    public function getEmbedURL() {
        if ($this->IsYoutube()) {
            return "//www.youtube.com/embed/" . self::youtubeId($this->value);
        } elseif ($this->IsVimeo()) {
            return "//player.vimeo.com/video/" . self::vimeoId($this->value);
        }
    }

    /**
     * @return string
     */
    public function URL() {
        if ($this->IsYoutube()) {
            return "https://www.youtube.com/watch?v=" . self::youtubeId($this->value);
        } elseif ($this->IsVimeo()) {
            return "https://vimeo.com/" . self::vimeoId($this->value);
        }
    }

    /**
     * @return bool
     */
    public function IsYoutube() {
        $host = parse_url($this->value, PHP_URL_HOST);
        return (strpos($host, 'youtube') !== false) || (strpos($host, 'youtu.be') !== false);
    }

    /**
     * @return bool
     */
    public function IsVimeo() {
        $host = parse_url($this->value, PHP_URL_HOST);
        return strpos($host, 'vimeo') !== false;
    }

    /**
     * @return bool
     */
    public static function youtubeId($url) {
        $regex = '~(?:(?:<iframe [^>]*src=")?|(?:(?:<object .*>)?(?:<param .*</param>)*(?:<embed [^>]*src=")?)?)?(?:https?:\/\/(?:[\w]+\.)*(?:youtu\.be/| youtube\.com| youtube-nocookie\.com)(?:\S*[^\w\-\s])?([\w\-]{11})[^\s]*)"?(?:[^>]*>)?(?:</iframe>|</embed></object>)?~ix';

        if(preg_match($regex, $url, $matches) && !empty($matches)) {
            return $matches[1];
        }

        return false;
    }

    /**
     * @return bool
     */
    public static function vimeoId($url) {
        $regex = '~(?:<iframe [^>]*src=")?(?:https?:\/\/(?:[\w]+\.)*vimeo\.com(?:[\/\w]*\/videos?)?\/([0-9]+)[^\s]*)"?(?:[^>]*></iframe>)?(?:<p>.*</p>)?~ix';

        if(preg_match($regex, $url, $matches) && !empty($matches)) {
            return $matches[1];
        }

        return false;
    }

    public function Thumbnail($size = 'large') {
        if ($this->value && $this->IsYoutube()) {
            if ($size == 'large')
                return 'https://img.youtube.com/vi/' . $this->getID() . '/hqdefault.jpg';
            if ($size == 'small')
                return 'https://img.youtube.com/vi/' . $this->getID() . '/1.jpg';
        } elseif ($this->value && $this->IsVimeo()) {
//            $cache = Cache::factory('VideoVarchar', 'Output', array('automatic_serialization' => true));
            $cache = Injector::inst()->get(CacheInterface::class . 'VideoVarchar', 'Output', array('automatic_serialization' => true));
            $cacheKey = md5($this->value);
            $arr = array();
            if ($content = $cache->load($cacheKey)) {
                $arr = unserialize($content);
            }
            else {
                try {
                    $content = @file_get_contents('http://vimeo.com/api/v2/video/' . $this->getID() . '.php');
                    $cache->save($content, $cacheKey);
                    $arr = unserialize($content);
                } catch (Exception $e) {}
            }
            if ($content) {
                if($size == 'small' && isset($arr[0]) && isset($arr[0]['thumbnail_small']))
                    return $arr[0]['thumbnail_small'];
                if($size == 'large' && isset($arr[0]) && isset($arr[0]['thumbnail_large']))
                    return $arr[0]['thumbnail_large'];
            }
        }
        return null;
    }

}

class VideoVarchar_Image extends ArrayData {

    private $url;

    public function setURL($url) {
        $this->url = $url;
        return $this;
    }

    public function forTemplate() {
        return "<div style='width: 50px; height: 50px; overflow: hidden; position: relative'>
			<img src=\"{$this->url}\" style='position: absolute; top: 50%; left: 50%; transform: translate(-50%, -50%); min-width: 100%; min-height: 100%;' /></div>";
    }

}