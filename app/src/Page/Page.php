<?php

namespace {

    use Sheadawson\Linkable\Forms\LinkField;
    use Sheadawson\Linkable\Models\Link;
    use SilverStripe\AssetAdmin\Forms\UploadField;
    use SilverStripe\Assets\Image;
    use SilverStripe\Blog\Model\Blog;
    use SilverStripe\Blog\Model\BlogPost;
    use SilverStripe\CMS\Model\SiteTree;
    use SilverStripe\Control\Controller;
    use SilverStripe\Forms\CheckboxField;
    use SilverStripe\Forms\DropdownField;
    use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
    use SilverStripe\Forms\TextField;
    use SilverStripe\ORM\PaginatedList;
    use SilverStripers\TrustMotors\Model\Element\ElementShowroom;
    use SilverStripers\TrustMotors\Presentation\Vehicle;

    class Page extends SiteTree
    {

        private static $db = [
            'HeroStyle' => 'Enum("SideImage, BgImage, NoBottomLinks", "SideImage")',
            'BottomMargin' => 'Enum("None, Medium, Large", "Medium")',
            'HeroTitle' => 'Text',
            'HeroContent' => 'HTMLText',
            'RemoveBlackLine' => 'Boolean',
        ];

        private static $has_one = [
            'HeroSideImage' => Image::class,
            'HeroBgImage' => Image::class,
            'HeroButtonLink' => Link::class,
            'BottomLinkOne' => Link::class,
            'BottomLinkTwo' => Link::class,
            'BottomLinkThree' => Link::class,
        ];

//        private static $many_many = [
//            'Links' => Link::class,
//        ];

        private static $owns = [
            'HeroSideImage',
            'HeroBgImage'
        ];

        public function getCMSFields()
        {
            $fields = parent::getCMSFields();
            if ($this->ClassName != BlogPost::class) {
                $fields->addFieldsToTab('Root.Hero.Main', [
                    TextField::create('HeroTitle', 'Title'),
                    HTMLEditorField::create('HeroContent', 'Content'),
                    LinkField::create('HeroButtonLinkID', 'Hero Link Button'),
                    UploadField::create('HeroSideImage', 'Side Image'),
                    UploadField::create('HeroBgImage', 'Background Image'),
                ]);
                $fields->addFieldsToTab('Root.Hero.Links', [
                    LinkField::create('BottomLinkOneID', 'Bottom Link One'),
                    LinkField::create('BottomLinkTwoID', 'Bottom Link Two'),
                    LinkField::create('BottomLinkThreeID', 'Bottom Link Three'),
                ]);
                $fields->addFieldsToTab('Root.Hero.Settings', [
                    DropdownField::create('HeroStyle', 'Style')->setSource([
                        'SideImage' => 'Side Image',
                        'BgImage' => 'Background Image',
                        'NoBottomLinks' => 'No Bottom Links'
                    ]),
                    DropdownField::create('BottomMargin', 'Bottom Margin')->setSource([
                        'None' => 'None',
                        'Medium' => 'Medium',
                        'Large' => 'Large',
                    ]),
                    CheckboxField::create('RemoveBlackLine', 'Remove black line?')
                ]);
            }
            return $fields;
        }

        public function HeroClass()
        {
            $classes = [];
            if ($this->HeroStyle == 'BgImage') {
                $classes[] = 'hero--bg-image';
            }
            if ($this->HeroStyle == 'NoBottomLinks') {
                $classes[] = 'hero--no-bottom-links';
            }
            $classes[] = 'margin-bottom-' . strtolower($this->BottomMargin);

            return implode(' ', $classes);
        }

    }
}
