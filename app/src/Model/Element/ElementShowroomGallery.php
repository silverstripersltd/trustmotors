<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 31/08/2020
 * Time: 14:10
 */

namespace SilverStripers\TrustMotors\Model\Element;


use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldAddNewButton;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\Forms\HeaderField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\FieldType\DBHTMLText;
use SilverStripers\TrustMotors\DB\VideoVarchar;
use SilverStripers\TrustMotors\DB\VideoVarchar_Image;
use SilverStripers\TrustMotors\Presentation\MediaObject;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class ElementShowroomGallery extends BaseElement
{
    private static $table_name = 'ElementShowroomGallery';

    private static $db = [
        'Cat1Title' => 'Varchar',
        'Cat2Title' => 'Varchar',
        'Cat3Title' => 'Varchar',
        'VideoLink1' => VideoVarchar::class,
        'VideoLink2' => VideoVarchar::class,
        'VideoLink3' => VideoVarchar::class,
    ];

    private static $has_one = [
        'Cat1Image' => Image::class,
        'Cat2Image' => Image::class,
        'Cat3Image' => Image::class,
    ];

    private static $owns = [
        'Cat1Image',
        'Cat2Image',
        'Cat3Image',
        'Cat1GalleryImages',
        'Cat2GalleryImages',
        'Cat3GalleryImages',
    ];

    private static $many_many = [
        'Cat1GalleryImages' => Image::class,
        'Cat2GalleryImages' => Image::class,
        'Cat3GalleryImages' => Image::class,
    ];

    public function getType()
    {
        return 'Showroom Gallery Block';
    }

    public function updateElementClass(&$classes)
    {
        $classes['block-showroom'] = 'block-showroom';
    }

    public function Thumbnail()
    {
        $image = new VideoVarchar_Image();
        $image->setURL($this->dbObject('VideoLink')->Thumbnail());
        return DBHTMLText::create()->setValue($image->forTemplate());
    }

    public function getVideoThumbnailLink($num)
    {
        return $this->dbObject('VideoLink'.$num)->Thumbnail();
    }

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->removeByName(['Cat1GalleryImages', 'Cat2GalleryImages', 'Cat3GalleryImages']);
        $fields->addFieldsToTab('Root.Category 1', [
            TextField::create('Cat1Title', 'Title'),
            UploadField::create('Cat1Image', 'Category Image'),
            HeaderField::create('Gallery files'),
            TextField::create('VideoLink1', 'Video link'),
            UploadField::create('Cat1GalleryImages', 'Gallery images')->setAllowedMaxFileNumber(3)
        ]);
        $fields->addFieldsToTab('Root.Category 2', [
            TextField::create('Cat2Title', 'Title'),
            UploadField::create('Cat2Image', 'Category Image'),
            HeaderField::create('Gallery files'),
            TextField::create('VideoLink2', 'Video link'),
            UploadField::create('Cat2GalleryImages', 'Gallery images')->setAllowedMaxFileNumber(3)
        ]);
        $fields->addFieldsToTab('Root.Category 3', [
            TextField::create('Cat3Title', 'Title'),
            UploadField::create('Cat3Image', 'Category Image'),
            HeaderField::create('Gallery files'),
            TextField::create('VideoLink3', 'Video link'),
            UploadField::create('Cat3GalleryImages', 'Gallery images')->setAllowedMaxFileNumber(3)
        ]);
//        $fields->insertAfter('Main', $fields->fieldByName('Category 3'));

        return $fields;
    }
}