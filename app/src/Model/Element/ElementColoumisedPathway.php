<?php
/**
 * Created by PhpStorm.
 * User: peru
 * Date: 2020-08-14
 * Time: 12:19
 */

namespace SilverStripers\TrustMotors\Model\Element;


use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripers\TrustMotors\Presentation\ColumnizedPathwayObject;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class ElementColoumisedPathway extends BaseElement
{
    private static $table_name = 'ElementColoumisedPathways';

    public function getType()
    {
        return 'Coloumised Pathways';
    }

    private static $has_many = [
        'PathwayObjects' => ColumnizedPathwayObject::class
    ];

    private static $owns = [
       'PathwayObjects'
    ];

    public function updateElementClass(&$classes)
    {
        $classes['block-coloumised-pathways'] = 'block-coloumised-pathways';
        unset($classes['reveal-up']);
    }

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->removeByName(['PathwayObjects']);
        $conf = GridFieldConfig_RecordEditor::create();
        $conf->addComponent(new GridFieldOrderableRows('Sort'));
        $fields->addFieldsToTab('Root.Main', [
            GridField::create('PathwayObjects', 'Pathways', $this->PathwayObjects(), $conf )
        ]);
        return $fields;
    }
}