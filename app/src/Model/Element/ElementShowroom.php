<?php
/**
 * Created by PhpStorm.
 * User: peru
 * Date: 2020-08-14
 * Time: 10:31
 */

namespace SilverStripers\TrustMotors\Model\Element;


use DNADesign\Elemental\Models\BaseElement;
use Sheadawson\Linkable\Forms\LinkField;
use Sheadawson\Linkable\Models\Link;
use SilverStripe\Control\Controller;
use SilverStripe\Control\Director;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\NumericField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataList;
use SilverStripe\ORM\PaginatedList;
use SilverStripers\TrustMotors\Controller\ElementShowRoomController;
use SilverStripers\TrustMotors\Presentation\BodyType;
use SilverStripers\TrustMotors\Presentation\FuelType;
use SilverStripers\TrustMotors\Presentation\Make;
use SilverStripers\TrustMotors\Presentation\Model;
use SilverStripers\TrustMotors\Presentation\ModelYear;
use SilverStripers\TrustMotors\Presentation\Transmission;
use SilverStripers\TrustMotors\Presentation\Vehicle;

class ElementShowroom extends BaseElement
{
    private static $table_name = 'ElementShowroom';

    private static $db = [
        'ShowPagination' => 'Boolean',
        'ShowFilters' => 'Boolean',
    ];

    private static $has_one = [
        'ShowLink' => Link::class,
    ];

    private $results = null;

    public function getType()
    {
        return 'Showroom Block';
    }

    public function updateElementClass(&$classes)
    {
        $classes['block-showroom'] = 'block-showroom';
    }

    public function getVehicles()
    {
        $controller = Controller::curr();
        $this->results = $controller->getVehicles();
        return $this->results;
    }

    public function getCurrentModelID()
    {
        $request = Controller::curr()->getRequest();
        return $request->getVar('Model');
    }

    public function getModels()
    {
        if (($makeID = $this->getCurrentMakeID()) && ($make = Make::get()->byID($makeID))) {
            $models = $make->BrandModels()->Sort('Title');
            $models = $this->setDependentFilters($models, 'Model');
            return $models;
        }
        return null;
    }

    public function getCurrentTransmissionID()
    {
        $request = Controller::curr()->getRequest();
        return $request->getVar('Transmission');
    }

    public function getTransmissions()
    {
        $list = Transmission::get()->Sort('Title');
        $list = $this->setDependentFilters($list, 'Transmission');
        return $list;
    }

    public function getCurrentMakeID()
    {
        $request = Controller::curr()->getRequest();
        return $request->getVar('Make');
    }

    public function getMakes()
    {
        $list = Make::get()->Sort('Title');
        $list = $this->setDependentFilters($list, 'Make');
//        echo $list->sql();die();
        return $list;
    }

    public function getCurrentYearID()
    {
        $request = Controller::curr()->getRequest();
        return $request->getVar('Year');
    }

    public function getYears()
    {
        $list = ModelYear::get()->Sort('Title');
        $list = $this->setDependentFilters($list, 'ModelYear');
        return $list;
    }

    public function getCurrentFuelTypeID()
    {
        $request = Controller::curr()->getRequest();
        return $request->getVar('FuelType');
    }

    public function getFuelTypes()
    {
        $list = FuelType::get()->Sort('Title');
        $list = $this->setDependentFilters($list, 'FuelType');
        return $list;
    }

    public function getCurrentBodyTypeID()
    {
        $request = Controller::curr()->getRequest();
        return $request->getVar('BodyType');
    }

    public function getBodyTypes()
    {
        $list = BodyType::get()->Sort('Title');
        $list = $this->setDependentFilters($list, 'BodyType');
//        echo $list->sql();die();
        return $list;
    }

    protected function setDependentFilters(DataList $list, $table)
    {
        $request = Controller::curr()->getRequest();
        $vars = $request->getVars();
        $list = $list->innerJoin('Vehicle', "Vehicle.{$table}ID = $table.ID");
        if (
            isset($vars['Make'])
            || isset($vars['Model'])
            || isset($vars['BodyType'])
            || isset($vars['FuelType'])
            || isset($vars['Year'])
            || isset($vars['Transmission'])
        ) {
            if (($makeID = $this->getCurrentMakeID()) && $table != 'Make') { // && $table != 'Make'
                $list = $list->where('Vehicle.MakeID = ' . $makeID);
            }
            if (($modelID = $this->getCurrentModelID()) && $table != 'Model') {
                $list = $list->where('Vehicle.ModelID = ' . $modelID);
            }
            if (($fID = $this->getCurrentFuelTypeID()) && $table != 'FuelType') {
                $list = $list->where('Vehicle.FuelTypeID = ' . $fID);
            }
            if (($bID = $this->getCurrentBodyTypeID()) && $table != 'BodyType') {
                $list = $list->where('Vehicle.BodyTypeID = ' . $bID);
            }
            if (($yID = $this->getCurrentYearID()) && $table != 'ModelYear') {
                $list = $list->where('Vehicle.ModelYearID = ' . $yID);
            }
            if (($tID = $this->getCurrentTransmissionID()) && $table != 'Transmission') {
                $list = $list->where('Vehicle.TransmissionID = ' . $tID);
            }
        }
        return $list;
    }

    public function getBaseLink(){
        $link = Director::absoluteBaseURL();
        if (strpos($link, '?') !== false) {
            return substr($link, 0, strpos($link, "?"));
        }else{
            return $link;
        }
    }

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->addFieldsToTab('Root.Main', [
            LinkField::create('ShowLinkID', 'Show All Link'),
            CheckboxField::create('ShowPagination'),
            CheckboxField::create('ShowFilters'),
        ]);
        return $fields;
    }

}