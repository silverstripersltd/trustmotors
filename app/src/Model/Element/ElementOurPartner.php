<?php
/**
 * Created by PhpStorm.
 * User: peru
 * Date: 2020-08-13
 * Time: 04:55
 */

namespace SilverStripers\TrustMotors\Model\Element;


use DNADesign\Elemental\Models\BaseElement;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RecordEditor;
use SilverStripe\Forms\GridField\GridFieldDeleteAction;
use SilverStripers\TrustMotors\Presentation\Logo;
use Symbiote\GridFieldExtensions\GridFieldOrderableRows;

class ElementOurPartner extends BaseElement
{
    private static $table_name = 'ElementOurPartners';

    private static $singular_name = 'Partners element';

    private static $plural_name = 'Partners elements';

    private static $description = 'Add a Partners block to the page';

    private static $inline_editable = false;

    private static $has_many = [
        'Logos' => Logo::class,
    ];

    private static $owns = [
        'Logos'
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->removeByName('Logos');
        $fields->addFieldToTab('Root.Main', GridField::create('Logos', 'Partner Logos')
            ->setList($this->Logos())
            ->setConfig(GridFieldConfig_RecordEditor::create(50)
                ->removeComponentsByType(GridFieldDeleteAction::class)
                ->addComponent(GridFieldOrderableRows::create('Sort'))));
        return $fields;
    }

    public function getType()
    {
        return 'Our Partners';
    }

    public function updateElementClass(&$classes)
    {
        $classes['block-partners'] = 'block-partners';
    }
}