<?php


namespace SilverStripers\TrustMotors\Model\Element;


use DNADesign\Elemental\Models\BaseElement;
use Sheadawson\Linkable\Forms\LinkField;
use Sheadawson\Linkable\Models\Link;
use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\TextField;

class ElementCalculator extends BaseElement
{
    private static $table_name = 'ElementCalculator';

    private static $db = [
        'RepaymentTitle' => 'Varchar(255)',
        'BorrowTitle' => 'Varchar(255)',
        'TermsConditions' => 'Varchar(255)',
    ];

    private static $has_one = [
        'ApplyNow' => Link::class,
    ];

    private static $description = 'Add a Calculator block to the page';

    private static $inline_editable = false;

    public function getType()
    {
        return 'Calculator';
    }

    public function updateElementClass(&$classes)
    {
        $classes['block-calculator'] = 'block-calculator';
    }

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->addFieldsToTab('Root.Main', [
            LinkField::create('ApplyNowID', 'Apply Now'),
            TextField::create('RepaymentTitle', 'Repayment Section Title'),
            TextField::create('BorrowTitle', 'Borrow Section Title'),
            TextField::create('TermsConditions', 'Terms Conditions Text'),
        ]);
        return $fields;
    }

}
