<?php

namespace {

    use SilverStripe\CMS\Controllers\ContentController;
    use SilverStripe\Control\Controller;
    use SilverStripe\ORM\PaginatedList;
    use SilverStripe\SiteConfig\SiteConfig;
    use SilverStripers\TrustMotors\Cronjob\MotorCentralImporter;
    use SilverStripers\TrustMotors\Model\Element\ElementShowroom;
    use SilverStripers\TrustMotors\Presentation\Make;
    use SilverStripers\TrustMotors\Presentation\Vehicle;

    class PageController extends ContentController
    {
        /**
         * An array of actions that can be accessed via a request. Each array element should be an action name, and the
         * permissions or conditions required to allow the user to access it.
         *
         * <code>
         * [
         *     'action', // anyone can access this action
         *     'action' => true, // same as above
         *     'action' => 'ADMIN', // you must have ADMIN permissions to access this action
         *     'action' => '->checkAction' // you can only access this action if $this->checkAction() returns true
         * ];
         * </code>
         *
         * @var array
         */
        private static $allowed_actions = [
            'filter',
            'models',
        ];


        protected function init()
        {
            parent::init();
            // You can include any CSS or JS required by your project here.
            // See: https://docs.silverstripe.org/en/developer_guides/templates/requirements/

//            $mc = new MotorCentralImporter();
//            $mc->process();
        }

        public function filter()
        {
            if (($id = $this->request->param('ID')) && $block = ElementShowroom::get()->byID($id)) {
                return json_encode([
                    'listings' => $this->customise(['Block' => $block])->renderWith([
                        'type' => 'Includes',
                        ElementShowroom::class . '_list',
                    ])->getValue(),
                    'pagination' => $this->renderWith([
                        'type' => 'Includes',
                        ElementShowroom::class . '_pagination',
                    ])->getValue(),
                    'filters' => $this->customise(['Block' => $block])->renderWith([
                        'type' => 'Includes',
                        ElementShowroom::class . '_filters',
                    ])->getValue()
                ]);
            }
            return null;
        }

        public function getVehicles()
        {
            $keys = $this->request->getVars();
            $list = Vehicle::get()->exclude('Hide', 1)->sort('LastUpdate', 'DESC');
            if (isset($keys['BodyType']) && $keys['BodyType']) {
                if($keys['BodyType'] != 'Body'){
                    $list = $list->filter('BodyTypeID', $keys['BodyType']);
                }
            }

            if (isset($keys['FuelType']) && $keys['FuelType']) {
                if($keys['FuelType'] != 'Fuel type'){
                    $list = $list->filter('FuelTypeID', $keys['FuelType']);
                }

            }

            if (isset($keys['Model']) && $keys['Model']) {
                if($keys['Model'] != 'Model'){
                    $list = $list->filter('ModelID', $keys['Model']);
                }
            }

            if (isset($keys['Make']) && $keys['Make']) {
                if($keys['Make'] != 'Make'){
                    $list = $list->filter('MakeID', $keys['Make']);
                }
            }

            if (isset($keys['Year']) && $keys['Year']) {
                if($keys['Year'] != 'Year'){
                    $list = $list->filter('ModelYearID', $keys['Year']);
                }
            }

            if (isset($keys['Transmission']) && $keys['Transmission']) {
                if($keys['Transmission'] != 'Transmission'){
                    $list = $list->filter('TransmissionID', $keys['Transmission']);
                }
            }
            $conf = SiteConfig::current_site_config();
            $paginated = new PaginatedList($list);
            $paginated->setRequest($this->getRequest());
            $paginated->setPageLength($conf->PageLength > 0 ? $conf->PageLength: 8);
            return $paginated;
        }

    }

}

