<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 10/11/2020
 * Time: 09:35
 */
namespace SilverStripers\TrustMotors\Controller;
use DNADesign\ElementalUserForms\Model\ElementForm;
use Google\Client;
use SilverStripe\Control\Controller;
use SilverStripe\Control\Director;
use SilverStripe\Core\Convert;
use SilverStripe\ORM\DB;

class GoogleOAuth extends Controller
{

    private static $url_segment = 'goauth';

    public function AbsoluteLink()
    {
        $link = parent::Link();
        return Director::absoluteURL($link);
    }

    public function getAppOAuth($id)
    {
        return $this->AbsoluteLink() . 'index/' . $id;
    }

    public function index()
    {

        $id = $this->request->param('ID');
        if ($id && ($element = ElementForm::get()->byID($id)) && $element->GoogleClientID && $element->GoogleClientSecret) {

            $googleClient = self::get_gclient($element);
            $googleClient->fetchAccessTokenWithAuthCode($this->request->requestVar('code'));

            $token = $googleClient->getAccessToken();
            if (!empty($token['access_token'])) {
                echo 'Updating to ' . $token['access_token'] . '<br>';
                $code = Convert::raw2sql($token['access_token']);
                DB::query('UPDATE ElementForm SET GoogleToken = \'' . $code . '\' WHERE ID = ' . $element->ID);
                DB::query('UPDATE ElementForm_Live SET GoogleToken = \'' . $code . '\' WHERE ID = ' . $element->ID);
            }
        }
        echo 'DONE';
        die();
    }

    /**
     * @param $element
     * @return Client
     */
    public static function get_gclient($element)
    {
        if($element->GoogleClientID && $element->GoogleClientSecret) {
            $ctrl = new GoogleOAuth();
            $googleClient = new Client();
            $googleClient->setApplicationName("Client_Library_Examples");
            $googleClient->setClientId($element->GoogleClientID);
            $googleClient->setClientSecret($element->GoogleClientSecret);
            $googleClient->setScopes(\Google_Service_Drive::DRIVE);
            $googleClient->setAccessType('offline');
            $googleClient->setRedirectUri($ctrl->getAppOAuth($element->ID));
            return $googleClient;
        }
        return null;

    }
}