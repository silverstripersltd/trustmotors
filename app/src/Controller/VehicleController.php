<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 18/08/2020
 * Time: 15:44
 */

namespace SilverStripers\TrustMotors\Controller;


use GuzzleHttp\Client;
use SilverStripe\Control\Director;
use SilverStripe\Control\Email\Email;
use SilverStripe\Control\HTTPRequest;
use SilverStripe\SiteConfig\SiteConfig;
use SilverStripe\View\ArrayData;
use SilverStripers\SEO\Extension\SEODataExtension;
use SilverStripers\TrustMotors\Presentation\EnquireSubmission;
use SilverStripers\TrustMotors\Presentation\Vehicle;

class VehicleController extends \PageController
{
    private static $allowed_actions = [
        'view',
        'enquireForm',
    ];


    public function view(HTTPRequest $request){
        $id = $request->param('ID');
        $vehicle = Vehicle::get()->byID($id);
        if($vehicle){
            SEODataExtension::override_seo_from($vehicle);
            return $this->customise(new ArrayData([
                'Vehicle' => $vehicle,
                'Listings' => $this->getSimilarListings($vehicle->Transmission()->ID, $vehicle->BodyType()->ID, $vehicle->ID),
                'Images' => $vehicle->Images(),
                'Phone' => $this->getPhone(),
                'FormLink' => $this->getFormLink(),
                'BaseLink' => $this->getBaseLink(),
            ]));
        }
        return $this->httpError(404);
    }

    public function getSimilarListings($transID, $bodyID, $ID)
    {
        return Vehicle::get()->filter(['TransmissionID' => $transID, 'BodyTypeID' => $bodyID, 'ID:not'=> $ID])->exclude('Hide' ,1)->sort('LastUpdate', 'DESC')->limit(4);
    }


    public function enquireForm(HTTPRequest $request)
    {
        $data = $request->getBody();
        $config = SiteConfig::current_site_config();
        $client = new Client();
        $res = $client->request('POST', 'https://dealers.virtualyard.com.au/api/v2/post.php?a=sendenquiry&key='.$config->VirtualYardAPI,
            ['form_params' => json_decode($data)]);
        if ($res->getStatusCode() == 200) {
//            $body = $res->getBody()->read(1024);
//            $arrayFormat = json_dncode($body);
            $data = json_decode($data);
            $vehicle = Vehicle::get()->filter('VehicleId',$data->vehicleId)->first();
            $enquireData = [
                'Name' => $data->fullname,
                'Email' => $data->email,
                'Phone' => $data->phone,
                'Message' => $data->message,
                'VehicleID' => $vehicle->ID,
            ];
            $submission = EnquireSubmission::create($enquireData);
            if($id = $submission->write() && $this->sendEmail($enquireData)){
                return json_encode(['Message' => 'Success']);
            }else{
                return json_encode(['Message' => 'Failed']);
            }
        }

        return json_encode(['Message' => 'Failed']);
    }

    public function sendEmail($data){
        $config = SiteConfig::current_site_config();
        $email = Email::create()
            ->setHTMLTemplate('Email\\SubscriptionEmail')
            ->setData($data)
            ->setFrom('noreply@trustmotors.co.nz')
            ->setTo($config->Email)
            ->setSubject('New Enquiry from '.$data['Name']);
        if ($email->send()) {
            return true;
        } else {
            return false;
        }
    }

    public function getPhone()
    {
        $conf = SiteConfig::current_site_config();
        return $conf->TestDrivePhone;
    }

    public function getFormLink()
    {
        $conf = SiteConfig::current_site_config();
        return $conf->FinanceLink();
    }

    public function getBaseLink(){
        $link = Director::absoluteBaseURL();
        if (strpos($link, '?') !== false) {
            return substr($link, 0, strpos($link, "?"));
        }else{
            return $link;
        }
    }

    public function getRate(){
        $conf = SiteConfig::current_site_config();
        return $conf->InterestRate;
    }

    public function getApprovalTime(){
        $conf = SiteConfig::current_site_config();
        return $conf->ApprovalIn;
    }

    public function getTerm(){
        $conf = SiteConfig::current_site_config();
        return $conf->Term;
    }

    public function getAdditionalCost(){
        $conf = SiteConfig::current_site_config();
        return $conf->AdditionalCost;
    }

    public function getAddressText(){
        $conf = SiteConfig::current_site_config();
        return $conf->AddressText;
    }
}