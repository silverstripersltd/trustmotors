<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 31/08/2020
 * Time: 13:48
 */

namespace SilverStripers\TrustMotors\Presentation;


use SilverStripe\ORM\DataObject;

class Transmission extends DataObject
{
    private static $table_name = 'Transmission';

    private static $db = [
        'Title' => 'Varchar'
    ];
}