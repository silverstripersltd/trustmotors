<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 31/08/2020
 * Time: 13:47
 */

namespace SilverStripers\TrustMotors\Presentation;


use SilverStripe\ORM\DataObject;

class Model extends DataObject
{
    private static $table_name = 'Model';

    private static $db = [
        'Title' => 'Varchar'
    ];

    private static $has_one = [
        'Make' => Make::class
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->removeByName('MakeID');
        return $fields;
    }
}