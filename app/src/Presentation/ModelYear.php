<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 31/08/2020
 * Time: 12:58
 */

namespace SilverStripers\TrustMotors\Presentation;


use SilverStripe\ORM\DataObject;

class ModelYear extends DataObject
{
    private static $table_name = 'ModelYear';

    private static $db = [
        'Title' => 'Varchar'
    ];
}