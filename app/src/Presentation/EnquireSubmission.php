<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 18/08/2020
 * Time: 15:17
 */

namespace SilverStripers\TrustMotors\Presentation;


use SilverStripe\Forms\ReadonlyField;
use SilverStripe\ORM\DataObject;

class EnquireSubmission extends DataObject
{
    private static $table_name = 'EnquireSubmission';

    private static $db =[
        'Name' => 'Varchar',
        'Email' => 'Varchar',
        'Phone' => 'Varchar',
        'Message' => 'Text',
    ];

    private static $has_one = [
        'Vehicle' => Vehicle::class
    ];

    private static $summary_fields = [
        'Name',
        'Email',
        'Phone',
        'Message',
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->removeByName('VehicleID');
        $fields->addFieldsToTab('Root.Main', [
            ReadonlyField::create('Name'),
            ReadonlyField::create('Email'),
            ReadonlyField::create('Phone'),
            ReadonlyField::create('Message'),
        ]);
        return $fields;
    }
}