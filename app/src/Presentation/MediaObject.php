<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 21/08/2020
 * Time: 10:50
 */

namespace SilverStripers\TrustMotors\Presentation;


use SilverStripe\Assets\Image;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\FieldType\DBHTMLText;
use SilverStripe\Versioned\Versioned;
use SilverStripers\TrustMotors\DB\VideoVarchar;
use SilverStripers\TrustMotors\DB\VideoVarchar_Image;
use SilverStripers\TrustMotors\Model\Element\ElementShowroomGallery;
use SilverStripers\TrustMotors\Page\ShowroomPage;

class MediaObject extends DataObject
{
    private static $table_name = 'MediaObject';

    private static $db = [
        'Sort' => 'Int',
        'VideoLink' => VideoVarchar::class,
    ];

    private static $extensions = [
        Versioned::class
    ];

    private static $has_one = [
        'Image' => Image::class,
        'ShowroomPage' => ShowroomPage::class,
    ];

    private static $owns = [
        'Image'
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->removeByName(['ShowroomPageID', 'Sort']);
        return $fields;
    }

    private static $summary_fields = [
        'Thumbnail',
        'VideoLink'
    ];

    public function Thumbnail()
    {
        if ($this->Image() && $this->Image()->exists()) {
            return $this->Image()->Fill(50, 50);
        }
        $image = new VideoVarchar_Image();
        $image->setURL($this->dbObject('VideoLink')->Thumbnail());
        return DBHTMLText::create()->setValue($image->forTemplate());
    }

    public function getVideoThumbnailLink()
    {
        if ($this->Image()->exists()) {
            return $this->Image()->getURL();
        }
        return $this->dbObject('VideoLink')->Thumbnail();
    }
}