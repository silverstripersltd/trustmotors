<?php
/**
 * Created by PhpStorm.
 * User: peru
 * Date: 2020-08-13
 * Time: 04:58
 */

namespace SilverStripers\TrustMotors\Presentation;


use Sheadawson\Linkable\Forms\LinkField;
use Sheadawson\Linkable\Models\Link;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;
use SilverStripe\Core\Manifest\ModuleResourceLoader;
use SilverStripe\ORM\DataObject;
use SilverStripe\ORM\FieldType\DBField;
use SilverStripe\Versioned\Versioned;
use SilverStripers\TrustMotors\Model\Element\ElementOurPartner;

class Logo extends DataObject
{
    private static $table_name = 'Logo';

    private static $db = [
        'Sort' => 'Int',
    ];

    private static $has_one = [
        'Image' => Image::class,
        'Link' => Link::class,
        'Element' => ElementOurPartner::class,
    ];

    private static $summary_fields = [
        'Thumbnail',
        'Link.LinkURL' => 'Link',
    ];

    private static $extensions = [
        Versioned::class
    ];

    private static $default_sort = 'Sort';

    private static $owns = [
        'Image'
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->removeByName(['LinkID', 'Sort', 'ElementID']);
        $fields->addFieldsToTab('Root.Main', [
            UploadField::create('Image', 'Image'),
            LinkField::create('LinkID', 'Link'),
        ]);
        return $fields;
    }

    public function getThumbnail()
    {
        return $this->Image()->Fill(50, 50);
    }
}