<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 18/08/2020
 * Time: 09:23
 */

namespace SilverStripers\TrustMotors\Presentation;


use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Versioned\Versioned;
use SilverStripers\TrustMotors\Model\Element\ElementColoumisedPathway;

class ColumnizedPathwayObject extends DataObject
{
    private static $table_name = 'ColumnizedPathwayObject';
    private static $db = [
        'Title' => 'Varchar',
        'Content' => 'HTMLText',
        'Title2' => 'Varchar',
        'Content2' => 'HTMLText',
        'HasBackground' => 'Boolean',
        'Sort' => 'Int',
    ];

    private static $has_one = [
        'ElementPathway' => ElementColoumisedPathway::class
    ];

    private static $summary_fields = [
        'Title' => '1st Column title',
        'Title2' => '2nd Column title',
    ];

    private static $extensions = [
        Versioned::class
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->removeByName(['ElementPathwayID', 'Sort']);
        $fields->addFieldsToTab('Root.Main', [
            CheckboxField::create('HasBackground', 'Has background color'),
            TextField::create('Title', 'Title 1'),
            HTMLEditorField::create('Content', 'Content 1'),
            TextField::create('Title2', 'Title 2'),
            HTMLEditorField::create('Content2', 'Content 2')
        ]);
        return $fields;
    }
}