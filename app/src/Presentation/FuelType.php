<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 31/08/2020
 * Time: 13:47
 */

namespace SilverStripers\TrustMotors\Presentation;


use SilverStripe\ORM\DataObject;

class FuelType extends DataObject
{
    private static $table_name = 'FuelType';

    private static $db = [
        'Title' => 'Varchar'
    ];
}