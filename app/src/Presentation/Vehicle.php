<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 18/08/2020
 * Time: 14:54
 */

namespace SilverStripers\TrustMotors\Presentation;


use SilverStripe\Assets\Image;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Versioned\Versioned;
use SilverStripers\SEO\Extension\SEODataExtension;

class Vehicle extends DataObject
{
    private static $table_name = 'Vehicle';

    private static $db = [
        'HashCode' => 'Text',
        'VehicleId' => 'Varchar(500)',
        'LocationName' => 'Varchar',
        'Variant' => 'Varchar',
        'Kms' => 'Varchar',
        'AdvKms' => 'Varchar',
        'PhotoPath' => 'Varchar',
        'Category' => 'Varchar',
        'Type' => 'Varchar',
        'EnginePower' => 'Varchar',
        'EngineMake' => 'Varchar',
        'EngineSize' => 'Varchar',
        'Description' => 'HTMLText',
        'ShortDescription' => 'Text',
        'PrevPrice' => 'Currency',
        'Price' => 'Currency',
        'LastUpdate' => 'Datetime',
        'IsDriveAway' => 'Varchar',
        'Contact' => 'Varchar',
        'Status' => 'Varchar',
        'RentalStatus' => 'Varchar',
        'Hide' => 'Boolean',
    ];

    private static $summary_fields = [
        'Make.Title' => 'Make',
        'Model.Title' => 'Model',
        'ModelYear.Title' => 'Year',
        'LocationName' => 'Location',
    ];

    private static $has_one = [
        'BodyType'  => BodyType::class,
        'FuelType'  => FuelType::class,
        'Make'      => Make::class,
        'Model'     => Model::class,
        'ModelYear' => ModelYear::class,
        'Transmission' => Transmission::class,
    ];

    private static $has_many = [
        'EnquireSubmissions' => EnquireSubmission::class
    ];

    private static $many_many = [
        'Images' => Image::class
    ];

    private static $extensions = [
        Versioned::class,
        SEODataExtension::class
    ];

    private static $owns = [
        'Images'
    ];

    public function Nice($value)
    {
        // return "<span title=\"$this->value\">$" . number_format($this->value, 2) . '</span>';
        $val = number_format(abs($value), 0);
        if ($value < 0) {
            return "($val)";
        }

        return $val;
    }

    public function getTitle()
    {
        return $this->Make()->Title.' '.$this->Model()->Title.' '.$this->ModelYear()->Title;
    }

    public static function nl2p($string, $line_breaks = true, $xml = true)
    {
        $string = str_replace(array('<p>', '</p>', '<br>', '<br />'), '', $string);
// It is conceivable that people might still want single line-breaks
// without breaking into a new paragraph.
        if ($line_breaks == true)
            return '<p>'.preg_replace(array("/([\n]{2,})/i", "/([^>])\n([^<])/i"), array("</p>\n<p>", '$1<br'.($xml == true ? ' /' : '').'>$2'), trim($string)).'</p>';
        else
            return '<p>'.preg_replace(
                    array("/([\n]{2,})/i", "/([\r\n]{3,})/i","/([^>])\n([^<])/i"),
                    array("</p>\n<p>", "</p>\n<p>", '$1<br'.($xml == true ? ' /' : '').'>$2'),
                    trim($string)).'</p>';
    }

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->removeByName(['HashCode', 'BodyTypeID', 'FuelTypeID', 'MakeID', 'ModelID', 'ModelYearID', 'TransmissionID']);
        return $fields;
    }

}