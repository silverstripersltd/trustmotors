<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 21/08/2020
 * Time: 10:20
 */

namespace SilverStripers\TrustMotors\Presentation;


use Sheadawson\Linkable\Forms\LinkField;
use Sheadawson\Linkable\Models\Link;
use SilverStripe\AssetAdmin\Forms\UploadField;
use SilverStripe\Assets\Image;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataObject;
use SilverStripe\Versioned\Versioned;
use SilverStripers\TrustMotors\Page\ShowroomPage;

class VehicleCategory extends DataObject
{
    private static $table_name = 'VehicleCategory';

    private static $db = [
        'Title' => 'Varchar',
        'Sort' => 'Int',
    ];

    private static $extensions = [
        Versioned::class
    ];

    private static $has_one = [
        'Image' => Image::class,
        'Link' => Link::class,
        'ShowroomPage' => ShowroomPage::class,
    ];

    private static $owns = [
        'Image'
    ];

    public function getCMSFields()
    {
        $fields = parent::getCMSFields();
        $fields->removeByName(['ShowroomPageID', 'Sort']);
        $fields->addFieldsToTab('Root.Main', [
            TextField::create('Title', 'Title'),
            LinkField::create('LinkID', 'Link'),
            UploadField::create('Image', 'Image'),
        ]);

        return $fields;
    }
}