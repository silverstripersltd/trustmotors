<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 31/08/2020
 * Time: 13:48
 */

namespace SilverStripers\TrustMotors\Presentation;


use SilverStripe\ORM\DataObject;

class BodyType extends DataObject
{
    private static $table_name = 'BodyType';

    private static $db = [
        'Title' => 'Varchar'
    ];
}