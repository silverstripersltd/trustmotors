<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 31/08/2020
 * Time: 13:46
 */

namespace SilverStripers\TrustMotors\Presentation;


use SilverStripe\ORM\DataObject;

class Make extends DataObject
{
    private static $table_name = 'Make';

    private static $db = [
        'Title' => 'Varchar'
    ];

    private static $has_many = [
        'BrandModels' => Model::class
    ];

}