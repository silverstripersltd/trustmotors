<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 10/11/2020
 * Time: 09:42
 */

namespace SilverStripers\TrustMotors\Extension;


use SilverStripe\Forms\CheckboxField;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataExtension;
use SilverStripers\TrustMotors\Controller\GoogleOAuth;

class FormExtension extends DataExtension
{

    private static $db = [
        'SubmitToGoogleSheets' => 'Boolean',
        'GoogleClientID' => 'Text',
        'GoogleClientSecret' => 'Text',
        'GoogleToken' => 'Text',
        'GoogleSheetID' => 'Text'
    ];


    public function updateCMSFields(FieldList $fields)
    {


        $fields->removeByName([
            'SubmitToGoogleSheets',
            'GoogleClientID',
            'GoogleClientSecret',
            'GoogleToken'
        ]);

        $googleOAuth = GoogleOAuth::create();

        $googleClient = GoogleOAuth::get_gclient($this->owner);


        $fields->addFieldsToTab('Root.GoogleDrive', [
            CheckboxField::create('SubmitToGoogleSheets'),
            TextField::create('GoogleClientID'),
            TextField::create('GoogleClientSecret')
                ->setDescription('Set redirect URL as: ' . $googleOAuth->getAppOAuth($this->owner->ID)),
            TextField::create('GoogleToken')->setReadonly(true),
            TextField::create('GoogleSheetID'),

        ]);
        if($googleClient) {
            $oauthURL = $googleClient->createAuthUrl();
            $oauth = LiteralField::create('AuthURL', sprintf('<p><a href="%s" target="_blank">Authenticate</a></p>', $oauthURL));
            $fields->insertAfter('GoogleSheetID', $oauth);
        }

    }
}