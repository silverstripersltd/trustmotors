<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 8/17/2020
 * Time: 11:45 AM
 */

namespace SilverStripers\TrustMotors\Extension;


use Sheadawson\Linkable\Forms\LinkField;
use Sheadawson\Linkable\Models\Link;
use SilverStripe\Forms\FieldList;
use SilverStripe\Forms\GridField\GridField;
use SilverStripe\Forms\GridField\GridFieldConfig_RelationEditor;
use SilverStripe\Forms\HeaderField;
use SilverStripe\Forms\HTMLEditor\HTMLEditorField;
use SilverStripe\Forms\LiteralField;
use SilverStripe\Forms\NumericField;
use SilverStripe\Forms\ReadonlyField;
use SilverStripe\Forms\TextareaField;
use SilverStripe\Forms\TextField;
use SilverStripe\ORM\DataExtension;

class SiteConfigExtension extends DataExtension
{
    private static $db = [
        'FooterInfo' => 'Varchar',
        'LinkedIn' => 'Varchar',
        'VirtualYardAPI' => 'Varchar',
        'TestDrivePhone' => 'Varchar',
        'InterestRate' => 'Decimal',
        'ApprovalIn' => 'Decimal',
        'AdditionalCost' => 'Decimal',
        'Term' => 'Decimal',
        'AddressText' => 'Text',
        'FBAccessToken' => 'Varchar',
        'FBAppID' => 'Varchar',
        'FBAppSecret' => 'Varchar',
        'FBPageID' => 'Varchar',
        'TitleAboveAddress' => 'Varchar',
        'HowToReachUs' => 'HTMLText',
        'PageLength' => 'Int',
    ];

    private static $defaults = [
        'InterestRate' => 16.50,
        'Term' => 5.00,
        'ApprovalIn' => 4.00,
    ];

    private static $many_many = [
      'FooterLinks' => Link::class,
    ];

    private static $has_one = [
        'FinanceLink' => Link::class,
    ];

    public function updateCustomCMSFields(FieldList $fields)
    {
        parent::updateCMSFields($fields);
        $conf = GridFieldConfig_RelationEditor::create();
        $fields->removeByName('LinkedIn');
        $fields->addFieldsToTab('Root.Settings.Footer', [
            $info = TextareaField::create('FooterInfo'),
            GridField::create('FooterLinks', 'Footer Links', $this->owner->FooterLinks(), $conf )
        ]);
        $fields->addFieldToTab('Root.Settings.Contacts',
            TextField::create('LinkedIn')->setDescription('e.g: https://linkedin.com/')
        , 'Instagram');
        $fields->addFieldToTab('Root.Settings.Contacts',
            HTMLEditorField::create('HowToReachUs')
        , 'Address');
        $fields->addFieldToTab('Root.Settings.Contacts',
            TextField::create('TitleAboveAddress')
        , 'Address');
        $fields->addFieldsToTab('Root.Main', [
            TextField::create('VirtualYardAPI', 'Virtual Yard API')->setDescription('your virtualyard.com api key'),
            TextField::create('FBAccessToken')->setDescription('your facebook page accesstoken key'),
            TextField::create('FBAppID')->setDescription('your facebook app id key'),
            TextField::create('FBAppSecret')->setDescription('your facebook app secret key'),
            TextField::create('FBPageID')->setDescription('your facebook page id'),
        ]);


        $fields->addFieldsToTab('Root.Vehicles', [
            LinkField::create('FinanceLinkID', 'Finance now form link'),
            TextField::create('TestDrivePhone')->setDescription('"Book a test drive" button phone number goes here'),
            NumericField::create('InterestRate', 'Interest rate')->setScale(2),
            NumericField::create('AdditionalCost', 'Additional cost'),
            NumericField::create('ApprovalIn', 'Approval in time (hours)')->setScale(2),
            NumericField::create('Term', 'Term (years)')->setScale(2),
            TextareaField::create('AddressText', 'Google map address'),
            NumericField::create('PageLength', 'No of vehicles per block')->setMaxLength(50),
        ]);
    }
}