<?php
/**
 * Created by PhpStorm.
 * User: peru
 * Date: 2020-08-11
 * Time: 09:18
 */

namespace SilverStripers\TrustMotors\Extension;


use SilverStripe\Control\Director;
use SilverStripe\Forms\DropdownField;
use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataExtension;

class BaseElementExtension extends DataExtension
{
    private static $db = [
        'BottomMargin' => 'Enum("None, Medium, Large", "Medium")',
    ];

    public function updateCMSFields(FieldList $fields)
    {
        parent::updateCMSFields($fields);
        $fields->addFieldToTab('Root.Settings', DropdownField::create('BottomMargin', 'Bottom Margin')->setSource([
            'None' => 'None',
            'Medium' => 'Medium',
            'Large' => 'Large',
        ]));
    }

    public function updateCSSOptions(&$options){
        unset($options['BackgroundColor']);
        unset($options['HeadingColor']);
        unset($options['Color']);
    }

    public function getElementClass()
    {
        $owner = $this->owner;
        $classes = [
            'block' => 'block',
            'reveal-up' => 'reveal-up',
            'margin-bottom' => 'margin-bottom-' . strtolower($owner->BottomMargin),
        ];

        $owner->invokeWithExtensions('updateElementClass', $classes);
        return implode(' ', $classes);
    }

    public function getOriginalPage()
    {
        return Director::get_current_page();
    }
}