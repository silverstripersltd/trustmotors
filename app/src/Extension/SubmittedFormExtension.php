<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 10/11/2020
 * Time: 09:46
 */

namespace SilverStripers\TrustMotors\Extension;


use DNADesign\ElementalUserForms\Model\ElementForm;
use REverse\GSheets\Client;
use REverse\GSheets\SpreadSheets;
use REverse\GSheets\Value;
use SilverStripe\ORM\DataExtension;
use SilverStripe\ORM\DataList;
use SilverStripers\TrustMotors\Controller\GoogleOAuth;
use SilverStripe\UserForms\Model\Submission\SubmittedForm;

class SubmittedFormExtension extends DataExtension
{

    public function updateAfterProcess()
	{
		/* @var $form SubmittedForm */
		$form = $this->owner;
		$parent = DataList::create($form->ParentClass)->byID($form->ParentID);

		if (is_a($parent, ElementForm::class) && $parent->SubmitToGoogleSheets && $parent->GoogleToken && $parent->GoogleSheetID) {
			try {
				$googleClient = GoogleOAuth::get_gclient($parent);
				$googleClient->setAccessToken($parent->GoogleToken);
				$client = new Client($googleClient);
				$spreadsheets = new SpreadSheets($client, $parent->GoogleSheetID);
				$fields = [];
				foreach ($form->Values() as $value) {
					$fields[] = $value->Value;
				}
				$value = new Value(...$fields);
				$spreadsheets->append($value, 'Sheet1', []);
			} catch (\Exception $exception) {
//			    echo $exception->getMessage();
//			    die();
            }
		}
	}
}
