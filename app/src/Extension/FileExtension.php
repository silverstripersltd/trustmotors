<?php

namespace SilverStripers\TrustMotors\Extension;

use SilverStripe\ORM\DataExtension;

class FileExtension extends DataExtension
{

    private static $db = [
        'ExternalID' => 'Varchar'
    ];

}