<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 05/11/2020
 * Time: 09:16
 */

namespace SilverStripers\TrustMotors\Extension;


use SilverStripe\ORM\DataExtension;
use SilverStripe\View\Requirements;

class UserDefinedFormControllerExtension extends DataExtension
{
    public function onAfterInit() {
        Requirements::block('silverstripe/userforms:client/thirdparty/jquery.are-you-sure/jquery.are-you-sure.js');
    }

}