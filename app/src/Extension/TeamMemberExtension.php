<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 28/08/2020
 * Time: 15:48
 */

namespace SilverStripers\TrustMotors\Extension;


use SilverStripe\Forms\EmailField;
use SilverStripe\Forms\FieldList;
use SilverStripe\ORM\DataExtension;

class TeamMemberExtension extends DataExtension
{
    private static $db = [
        'Mail' => 'Varchar'
    ];

    public function updateCMSFields(FieldList $fields)
    {
        parent::updateCMSFields($fields);
        $fields->addFieldToTab('Root.Main', EmailField::create('Mail', 'E-Mail address'));
    }
}