<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 28/08/2020
 * Time: 13:12
 */

namespace SilverStripers\TrustMotors\Extension;


use SilverStripe\ORM\DataExtension;

class ElementVirtualExtension extends DataExtension {
    public function updateElementClass(&$classes)
    {
        $element = $this->owner->LinkedElement();
        if($element){
            switch ($element->getType()){
                case 'Our Partners':{
                    $classes['block-partners'] = 'block-partners';
                    break;
                }
                case 'Showroom Block':{
                    $classes['block-showroom'] = 'block-showroom';
                    break;
                }
                case 'Coloumised Pathways':{
                    $classes['block-coloumised-pathways'] = 'block-coloumised-pathways';
                    unset($classes['reveal-up']);
                    break;
                }
                case 'Video':{
                    $classes['block-video'] = 'block-video';
                    break;
                }
                case 'Two column content':{
                    $classes['block-two-column-content'] = 'block-two-column-content';
                    break;
                }
                case 'Testimonials':{
                    $classes['block-testimonials'] = 'block-testimonials';
                    break;
                }
                case 'Team':{
                    $classes['block-team'] = 'block-team';
                    unset($classes['reveal-up']);
                    break;
                }
                case 'Tabs':{
                    $classes['block-tabs'] = 'block-tabs';
                    break;
                }
                case 'Project List':{
                    $classes['block-project-list'] = 'block-project-list';
                    unset($classes['reveal-up']);
                    break;
                }
                case 'Pathways':{
                    $classes['block-pathways'] = 'block-pathways';
                    unset($classes['reveal-up']);
                    break;
                }
                case 'Pathway Carousel':{
                    $classes['block-pathway-carousel'] = 'block-pathway-carousel';
                    break;
                }
                case 'Options':{
                    $classes['block-product-option'] = 'block-product-option';
                    break;
                }
                case 'Map':{
                    $classes['block-map'] = 'block-map';
                    break;
                }
                case 'Icons Block':{
                    $classes['block-icon'] = 'block-icon';
                    if ($element->IconType == 'Number') {
                        $classes[] = 'block-icon--' . strtolower($element->IconType);
                    }
                    unset($classes['reveal-up']);
                    break;
                }
                case 'Half Box':{
                    $classes['block-halfbox'] = 'block-halfbox';
                    $classes[] = 'block-halfbox--image-' . strtolower($element->ImagePosition);
                    if ($element->RemoveMobileMarginBottom == 'None') {
                        $classes[] = 'mobile-margin-bottom-' . strtolower($element->RemoveMobileMarginBottom);
                    }
                    break;
                }
                case 'Frequently Asked Questions':{
                    $classes['block-faq'] = 'block-faq';
                    break;
                }
                case 'Exceprt':{
                    $classes['block-excerpt'] = 'block-excerpt';
                    break;
                }
                case 'Call to action':{
                    $classes['block-cta'] = 'block-cta';
                    if ($element->BackgroundStyle == 'Black') {
                        $classes[] = 'block-cta--bg-' . strtolower($element->BackgroundStyle);
                    }
                    break;
                }
                case 'Content With Image':{
                    $classes['block-content-with-image'] = 'block-content-with-image';
                    $classes[] = 'block-content-with-image--image-' . strtolower($element->ImagePosition);
                    break;
                }
                case 'Blog Post':{
                    $classes['block-blog-post'] = 'block-blog-post';
                    break;
                }

                default: {
                    $classes['virtual'] = 'virtual';
                    break;
                }
            }
        }
    }
}