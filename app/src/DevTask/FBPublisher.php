<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 24/08/2020
 * Time: 11:13
 */

namespace SilverStripers\TrustMotors\DevTask;

use Facebook\Authentication\AccessToken;
use Facebook\Exceptions\FacebookResponseException;
use Facebook\Exceptions\FacebookSDKException;
use Facebook\Facebook;
use GuzzleHttp\Client;
use SilverStripe\Control\Director;
use SilverStripe\Dev\BuildTask;
use SilverStripe\ORM\ValidationException;
use SilverStripe\Security\Permission;
use SilverStripe\Security\Security;
use SilverStripe\SiteConfig\SiteConfig;
class FBPublisher extends BuildTask
{
    private static $segment = 'FacebookPublisher';

    protected $title = 'Facebook Publisher';

    public function init()
    {
        if (!Permission::check('ADMIN')) {
            return Security::permissionFailure();
        }
        parent::init();
    }

    public function getDescription()
    {
        return _t(
            __CLASS__ . '.Description',
            'A task used to publish Virtualyard vehicles on the facebook page'
        );
    }

    public function run($request)
    {
        ini_set('max_execution_time', '1200'); //300 seconds = 5 minutes
        if (!$request->requestVar('confirm')) {
            $action = Director::baseURL() . 'dev/tasks/FacebookPublisher';
            $html = <<<HTML
<form method="post" enctype="multipart/form-data" action="$action">
            <label>This action can not be undone</label></br>
            <div><input type="submit" name="confirm" value="Run"></div>
            
</form> 
HTML;
            echo $html;
        } else {


            $config = SiteConfig::current_site_config();

            if($config->FBAppID && $config->FBAppSecret && $config->VirtualYardAPI && $config->FBPageID){
                try {
                    if (!$config->FBAccessToken) {
                        $link = 'https://developers.facebook.com/tools/explorer/';
                        $instructions = 'https://docs.google.com/document/d/1-HyPt9quS089UTFbV22Cl3pLagxkpRea5E7HeLPfKnE/edit?usp=sharing';
                        throw new ValidationException(
                            'You have no FB access token or existing one is expired. Please click <a href="' . $link . '" target="_blank">here</a> to generate one. <a href="' . $instructions . '" target="_blank">Get instructions</a>'
                        );
                    }
                    else{
                        $fbAccessToken = new AccessToken($config->FBAccessToken);
                        if (!$fbAccessToken->isExpired()) {
                            $fb = new Facebook([
                                'app_id' => $config->FBAppID,
                                'app_secret' => $config->FBAppSecret,
                                'default_graph_version' => 'v8.0',
                            ]);

                            $client = new Client();
                            $res = $client->request('GET', 'https://dealers.virtualyard.com.au/api/v2/get.php?a=vehicles&key=' . $config->VirtualYardAPI);
                            if ($res->getStatusCode() == 200) {
                                $jsonOutput = $res->getBody();
                                $arrayFormat = \GuzzleHttp\json_decode($jsonOutput);
                                if (count($arrayFormat->response) > 0) {
                                    foreach ($arrayFormat->response as $el) {
                                        $photoIdArray = array();
                                        if (property_exists($el, "photo1") && property_exists($el, "photoPath")) {
                                            for ($x = 1; $x <= 50; $x++) {
                                                if (property_exists($el, "photo" . $x) && property_exists($el, "photoPath")) {
                                                    $name = 'photo' . $x;
                                                    if ($el->photoPath && $el->$name && $this->url_exists($el->photoPath . $el->$name)) {

                                                        $photoIdArray[] = $this->uploadPhoto($fb, $el->photoPath . $el->$name);
                                                    }
                                                } else {
                                                    break;
                                                }
                                            }
                                            $this->publishMultiPhotoStory($fb, $this->checkProperty($el, ['make', 'model', 'year', 'transmission', 'variant', 'fuelType', 'type', 'category']), $photoIdArray);
                                        }
                                    }
                                }
                            } else {
                                return 'Failed. Internal server error occurred.';
                            }
                            echo '<pre>All vehicles are successfully published on the facebook page';

                        }else{
                            try {
                                $link = 'https://developers.facebook.com/tools/explorer/';
                                $instructions = 'https://docs.google.com/document/d/1-HyPt9quS089UTFbV22Cl3pLagxkpRea5E7HeLPfKnE/edit?usp=sharing';
                                throw new ValidationException(
                                    'You have no FB access token or existing one is expired. Please click <a href="' . $link . '" target="_blank">here</a> to generate one. <a href="' . $instructions . '" target="_blank">Get instructions</a>'
                                );
                            }catch(ValidationException $e) {
                                echo $e->getMessage();
                            }
                        }
                    }
                }catch(ValidationException $e) {
                    echo $e->getMessage();
                }

            }
            echo '<pre>Please check and add the correct keys(AppID, Access-token, App secret, PageID, Virtualyard API key) in CMS settings';
        }
    }

    function publishMultiPhotoStory($fb, $caption, $photoIdArray)
    {
        $config = SiteConfig::current_site_config();
        $params = array( "message" => $caption );
        foreach($photoIdArray as $k => $photoId) {
            $params["attached_media"][$k] = '{"media_fbid":"' . $photoId . '"}';
        }
        try {
            $postResponse = $fb->post('/' . $config->FBPageID . '/feed', $params, $config->FBAccessToken);
        } catch (FacebookResponseException $e) {
            print $e->getMessage();
            exit();
        } catch (FacebookSDKException $e) {
            print $e->getMessage();
            exit();
        }

    }

    function uploadPhoto($fb, $photoPath)
    {
        $config = SiteConfig::current_site_config();
        try {
            $response = $fb->post(
                '/' . $config->FBPageID . '/photos',
                array(
                    'url' => $photoPath,
                    "published" => false
                ), $config->FBAccessToken
            );
            $photoId = $response->getDecodedBody();
            if(!empty($photoId["id"])) {
                return $photoId["id"];
            }

        } catch (FacebookResponseException $e) {
            echo 'Graph returned an error: ' . $e->getMessage();
            exit;
        } catch (FacebookSDKException $e) {
            echo 'Facebook SDK returned an error: ' . $e->getMessage();
            exit;
        }
    }

    public function checkProperty($obj, $array){
        $name = '';
        if(property_exists($obj, 'year')){
            $name = $name.$obj->year.' ';
        }

        if(property_exists($obj, 'make')){
            $name = $name.$obj->make.' ';
        }

        if(property_exists($obj, 'model')){
            $name = $name.$obj->model.' ';
        }

        if(property_exists($obj, 'transmission')){
            $name = $name.$obj->transmission.' ';
        }

        if(property_exists($obj, 'shortDescription')){
            $name = $name.$obj->shortDescription.'
            ';
        }
        foreach($array as $val){
            $name .= property_exists($obj, $val) ? ' '.str_replace(' ', '_', '#'.$obj->$val).' ' : '';
        }
        return $name;
    }

    function url_exists($url) {
        $file = $url;
        $file_headers = @get_headers($file);
        if(!$file_headers || $file_headers[0] == 'HTTP/1.1 404 Not Found') {
            return false;
        }
        else {
            return true;
        }
    }

}