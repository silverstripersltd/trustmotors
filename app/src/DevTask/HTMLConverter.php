<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 09/11/2020
 * Time: 13:30
 */

namespace SilverStripers\TrustMotors\DevTask;


use SilverStripe\Control\Director;
use SilverStripe\Dev\BuildTask;
use SilverStripe\Security\Permission;
use SilverStripe\Security\Security;
use SilverStripers\TrustMotors\Presentation\Vehicle;

class HTMLConverter extends BuildTask
{
    private static $segment = 'HTMLConverter';

    protected $title = 'HTML Converter';

    public function init()
    {
        if (!Permission::check('ADMIN')) {
            return Security::permissionFailure();
        }
        parent::init();
    }

    public function getDescription()
    {
        return _t(
            __CLASS__ . '.Description',
            'A task used to convert vehicle Description in to html'
        );
    }

    public function run($request)
    {
        ini_set('max_execution_time', '1200'); //300 seconds = 5 minutes
        if (!$request->requestVar('confirm')) {
            $action = Director::baseURL() . 'dev/tasks/HTMLConverter';
            $html = <<<HTML
<form method="post" enctype="multipart/form-data" action="$action">
            <label>This action can not be undone</label></br>
            <div><input type="submit" name="confirm" value="Run"></div>
            
</form> 
HTML;
            echo $html;
        } else {

            foreach (Vehicle::get() as $vehicle) {
                $vehicle->Description = Vehicle::nl2p($vehicle->Description);
                $vehicle->write();
                $vehicle->publishRecursive();
            }

            echo '<pre>All Descriptions are converted to HTML successfully!';

        }
    }

}