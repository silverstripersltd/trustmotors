<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 19/08/2020
 * Time: 12:40
 */

namespace SilverStripers\TrustMotors\Admin;


use SilverStripe\Admin\ModelAdmin;
use SilverStripers\TrustMotors\Presentation\Vehicle;

class VehicleAdmin extends ModelAdmin
{
    private static $url_segment = 'vehicles';

    private static $menu_title = 'Vehicles';

    private static $menu_icon_class = 'font-icon-database';

    private static $managed_models = array(
        Vehicle::class
    );

    public $showImportForm = false;

    public function getList()
    {
        $list = parent::getList();
        return $list;
    }

    public function getEditForm($id = null, $fields = null)
    {
        $form = parent::getEditForm($id, $fields);
        return $form;
    }
}