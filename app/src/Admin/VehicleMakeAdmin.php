<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 02/09/2020
 * Time: 17:35
 */

namespace SilverStripers\TrustMotors\Admin;


use SilverStripe\Admin\ModelAdmin;
use SilverStripers\TrustMotors\Presentation\Make;

class VehicleMakeAdmin extends ModelAdmin
{
    private static $url_segment = 'models';

    private static $menu_title = 'Brands';

    private static $menu_icon_class = 'font-icon-circle-star';

    private static $managed_models = array(
        Make::class
    );

    public $showImportForm = false;

    public function getList()
    {
        $list = parent::getList();
        return $list;
    }

    public function getEditForm($id = null, $fields = null)
    {
        $form = parent::getEditForm($id, $fields);
        return $form;
    }
}