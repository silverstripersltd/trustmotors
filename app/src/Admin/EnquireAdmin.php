<?php
/**
 * Created by PhpStorm.
 * User: acer
 * Date: 18/08/2020
 * Time: 15:26
 */

namespace SilverStripers\TrustMotors\Admin;


use SilverStripe\Admin\ModelAdmin;
use SilverStripers\TrustMotors\Presentation\EnquireSubmission;

class EnquireAdmin extends ModelAdmin
{
    private static $url_segment = 'submissions';

    private static $menu_title = 'Submissions';

    private static $menu_icon_class = 'font-icon-comment';

    private static $managed_models = array(
        EnquireSubmission::class
    );

    public $showImportForm = false;

    public function getList()
    {
        $list = parent::getList();
        return $list;
    }

    public function getEditForm($id = null, $fields = null)
    {
        $form = parent::getEditForm($id, $fields);
        return $form;
    }
}